package com.ooad.bookstore.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ImportDetails.
 */
@Entity
@Table(name = "import_details")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "importdetails")
public class ImportDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "import_price")
    private Float importPrice;

    @OneToOne
    @JoinColumn(unique = true)
    private Book book;

    @ManyToOne
    @JsonIgnoreProperties("importDetails")
    private ImportBook importBook;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public ImportDetails amount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Float getImportPrice() {
        return importPrice;
    }

    public ImportDetails importPrice(Float importPrice) {
        this.importPrice = importPrice;
        return this;
    }

    public void setImportPrice(Float importPrice) {
        this.importPrice = importPrice;
    }

    public Book getBook() {
        return book;
    }

    public ImportDetails book(Book book) {
        this.book = book;
        return this;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public ImportBook getImportBook() {
        return importBook;
    }

    public ImportDetails importBook(ImportBook importBook) {
        this.importBook = importBook;
        return this;
    }

    public void setImportBook(ImportBook importBook) {
        this.importBook = importBook;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImportDetails)) {
            return false;
        }
        return id != null && id.equals(((ImportDetails) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ImportDetails{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", importPrice=" + getImportPrice() +
            "}";
    }
}
