package com.ooad.bookstore.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A ImportBook.
 */
@Entity
@Table(name = "import_book")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "importbook")
public class ImportBook implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "import_date")
    private Instant importDate;

    @Column(name = "provider")
    private String provider;

    @OneToOne
    @JoinColumn(unique = true)
    private Employee employee;

    @OneToMany(mappedBy = "importBook")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ImportDetails> importDetails = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getImportDate() {
        return importDate;
    }

    public ImportBook importDate(Instant importDate) {
        this.importDate = importDate;
        return this;
    }

    public void setImportDate(Instant importDate) {
        this.importDate = importDate;
    }

    public String getProvider() {
        return provider;
    }

    public ImportBook provider(String provider) {
        this.provider = provider;
        return this;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Employee getEmployee() {
        return employee;
    }

    public ImportBook employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Set<ImportDetails> getImportDetails() {
        return importDetails;
    }

    public ImportBook importDetails(Set<ImportDetails> importDetails) {
        this.importDetails = importDetails;
        return this;
    }

    public ImportBook addImportDetails(ImportDetails importDetails) {
        this.importDetails.add(importDetails);
        importDetails.setImportBook(this);
        return this;
    }

    public ImportBook removeImportDetails(ImportDetails importDetails) {
        this.importDetails.remove(importDetails);
        importDetails.setImportBook(null);
        return this;
    }

    public void setImportDetails(Set<ImportDetails> importDetails) {
        this.importDetails = importDetails;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImportBook)) {
            return false;
        }
        return id != null && id.equals(((ImportBook) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ImportBook{" +
            "id=" + getId() +
            ", importDate='" + getImportDate() + "'" +
            ", provider='" + getProvider() + "'" +
            "}";
    }
}
