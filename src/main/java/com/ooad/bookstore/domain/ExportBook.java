package com.ooad.bookstore.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A ExportBook.
 */
@Entity
@Table(name = "export_book")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "exportbook")
public class ExportBook implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "export_date")
    private Instant exportDate;

    @Column(name = "total_amount")
    private Float totalAmount;

    @OneToOne
    @JoinColumn(unique = true)
    private Employee employee;

    @OneToOne
    @JoinColumn(unique = true)
    private Customer customer;

    @OneToMany(mappedBy = "exportBook")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ExportDetails> importDetails = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getExportDate() {
        return exportDate;
    }

    public ExportBook exportDate(Instant exportDate) {
        this.exportDate = exportDate;
        return this;
    }

    public void setExportDate(Instant exportDate) {
        this.exportDate = exportDate;
    }

    public Float getTotalAmount() {
        return totalAmount;
    }

    public ExportBook totalAmount(Float totalAmount) {
        this.totalAmount = totalAmount;
        return this;
    }

    public void setTotalAmount(Float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Employee getEmployee() {
        return employee;
    }

    public ExportBook employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Customer getCustomer() {
        return customer;
    }

    public ExportBook customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<ExportDetails> getImportDetails() {
        return importDetails;
    }

    public ExportBook importDetails(Set<ExportDetails> exportDetails) {
        this.importDetails = exportDetails;
        return this;
    }

    public ExportBook addImportDetails(ExportDetails exportDetails) {
        this.importDetails.add(exportDetails);
        exportDetails.setExportBook(this);
        return this;
    }

    public ExportBook removeImportDetails(ExportDetails exportDetails) {
        this.importDetails.remove(exportDetails);
        exportDetails.setExportBook(null);
        return this;
    }

    public void setImportDetails(Set<ExportDetails> exportDetails) {
        this.importDetails = exportDetails;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExportBook)) {
            return false;
        }
        return id != null && id.equals(((ExportBook) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ExportBook{" +
            "id=" + getId() +
            ", exportDate='" + getExportDate() + "'" +
            ", totalAmount=" + getTotalAmount() +
            "}";
    }
}
