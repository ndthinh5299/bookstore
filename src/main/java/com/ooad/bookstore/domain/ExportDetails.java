package com.ooad.bookstore.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ExportDetails.
 */
@Entity
@Table(name = "export_details")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "exportdetails")
public class ExportDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "import_price")
    private Float importPrice;

    @OneToOne
    @JoinColumn(unique = true)
    private Book book;

    @ManyToOne
    @JsonIgnoreProperties("importDetails")
    private ExportBook exportBook;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public ExportDetails amount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Float getImportPrice() {
        return importPrice;
    }

    public ExportDetails importPrice(Float importPrice) {
        this.importPrice = importPrice;
        return this;
    }

    public void setImportPrice(Float importPrice) {
        this.importPrice = importPrice;
    }

    public Book getBook() {
        return book;
    }

    public ExportDetails book(Book book) {
        this.book = book;
        return this;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public ExportBook getExportBook() {
        return exportBook;
    }

    public ExportDetails exportBook(ExportBook exportBook) {
        this.exportBook = exportBook;
        return this;
    }

    public void setExportBook(ExportBook exportBook) {
        this.exportBook = exportBook;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExportDetails)) {
            return false;
        }
        return id != null && id.equals(((ExportDetails) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ExportDetails{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", importPrice=" + getImportPrice() +
            "}";
    }
}
