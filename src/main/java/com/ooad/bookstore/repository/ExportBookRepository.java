package com.ooad.bookstore.repository;

import com.ooad.bookstore.domain.ExportBook;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ExportBook entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExportBookRepository extends JpaRepository<ExportBook, Long>, JpaSpecificationExecutor<ExportBook> {
}
