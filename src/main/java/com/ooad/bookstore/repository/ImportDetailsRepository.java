package com.ooad.bookstore.repository;

import com.ooad.bookstore.domain.ImportDetails;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ImportDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImportDetailsRepository extends JpaRepository<ImportDetails, Long>, JpaSpecificationExecutor<ImportDetails> {
}
