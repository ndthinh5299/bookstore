package com.ooad.bookstore.repository;

import com.ooad.bookstore.domain.ImportBook;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ImportBook entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImportBookRepository extends JpaRepository<ImportBook, Long>, JpaSpecificationExecutor<ImportBook> {
}
