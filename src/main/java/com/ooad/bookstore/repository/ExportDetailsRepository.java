package com.ooad.bookstore.repository;

import com.ooad.bookstore.domain.ExportDetails;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ExportDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExportDetailsRepository extends JpaRepository<ExportDetails, Long>, JpaSpecificationExecutor<ExportDetails> {
}
