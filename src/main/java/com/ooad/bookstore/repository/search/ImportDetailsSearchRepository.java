package com.ooad.bookstore.repository.search;

import com.ooad.bookstore.domain.ImportDetails;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ImportDetails} entity.
 */
public interface ImportDetailsSearchRepository extends ElasticsearchRepository<ImportDetails, Long> {
}
