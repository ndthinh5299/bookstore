package com.ooad.bookstore.repository.search;

import com.ooad.bookstore.domain.ExportBook;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ExportBook} entity.
 */
public interface ExportBookSearchRepository extends ElasticsearchRepository<ExportBook, Long> {
}
