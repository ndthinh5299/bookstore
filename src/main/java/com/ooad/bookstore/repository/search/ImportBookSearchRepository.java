package com.ooad.bookstore.repository.search;

import com.ooad.bookstore.domain.ImportBook;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ImportBook} entity.
 */
public interface ImportBookSearchRepository extends ElasticsearchRepository<ImportBook, Long> {
}
