package com.ooad.bookstore.repository.search;

import com.ooad.bookstore.domain.ExportDetails;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ExportDetails} entity.
 */
public interface ExportDetailsSearchRepository extends ElasticsearchRepository<ExportDetails, Long> {
}
