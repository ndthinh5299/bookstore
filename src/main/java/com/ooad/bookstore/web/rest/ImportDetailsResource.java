package com.ooad.bookstore.web.rest;

import com.ooad.bookstore.service.ImportDetailsService;
import com.ooad.bookstore.web.rest.errors.BadRequestAlertException;
import com.ooad.bookstore.service.dto.ImportDetailsDTO;
import com.ooad.bookstore.service.dto.ImportDetailsCriteria;
import com.ooad.bookstore.service.ImportDetailsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.ooad.bookstore.domain.ImportDetails}.
 */
@RestController
@RequestMapping("/api")
public class ImportDetailsResource {

    private final Logger log = LoggerFactory.getLogger(ImportDetailsResource.class);

    private static final String ENTITY_NAME = "importDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ImportDetailsService importDetailsService;

    private final ImportDetailsQueryService importDetailsQueryService;

    public ImportDetailsResource(ImportDetailsService importDetailsService, ImportDetailsQueryService importDetailsQueryService) {
        this.importDetailsService = importDetailsService;
        this.importDetailsQueryService = importDetailsQueryService;
    }

    /**
     * {@code POST  /import-details} : Create a new importDetails.
     *
     * @param importDetailsDTO the importDetailsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new importDetailsDTO, or with status {@code 400 (Bad Request)} if the importDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/import-details")
    public ResponseEntity<ImportDetailsDTO> createImportDetails(@RequestBody ImportDetailsDTO importDetailsDTO) throws URISyntaxException {
        log.debug("REST request to save ImportDetails : {}", importDetailsDTO);
        if (importDetailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new importDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ImportDetailsDTO result = importDetailsService.save(importDetailsDTO);
        return ResponseEntity.created(new URI("/api/import-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /import-details} : Updates an existing importDetails.
     *
     * @param importDetailsDTO the importDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated importDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the importDetailsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the importDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/import-details")
    public ResponseEntity<ImportDetailsDTO> updateImportDetails(@RequestBody ImportDetailsDTO importDetailsDTO) throws URISyntaxException {
        log.debug("REST request to update ImportDetails : {}", importDetailsDTO);
        if (importDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ImportDetailsDTO result = importDetailsService.save(importDetailsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, importDetailsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /import-details} : get all the importDetails.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of importDetails in body.
     */
    @GetMapping("/import-details")
    public ResponseEntity<List<ImportDetailsDTO>> getAllImportDetails(ImportDetailsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ImportDetails by criteria: {}", criteria);
        Page<ImportDetailsDTO> page = importDetailsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /import-details/count} : count all the importDetails.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/import-details/count")
    public ResponseEntity<Long> countImportDetails(ImportDetailsCriteria criteria) {
        log.debug("REST request to count ImportDetails by criteria: {}", criteria);
        return ResponseEntity.ok().body(importDetailsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /import-details/:id} : get the "id" importDetails.
     *
     * @param id the id of the importDetailsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the importDetailsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/import-details/{id}")
    public ResponseEntity<ImportDetailsDTO> getImportDetails(@PathVariable Long id) {
        log.debug("REST request to get ImportDetails : {}", id);
        Optional<ImportDetailsDTO> importDetailsDTO = importDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(importDetailsDTO);
    }

    /**
     * {@code DELETE  /import-details/:id} : delete the "id" importDetails.
     *
     * @param id the id of the importDetailsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/import-details/{id}")
    public ResponseEntity<Void> deleteImportDetails(@PathVariable Long id) {
        log.debug("REST request to delete ImportDetails : {}", id);
        importDetailsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/import-details?query=:query} : search for the importDetails corresponding
     * to the query.
     *
     * @param query the query of the importDetails search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/import-details")
    public ResponseEntity<List<ImportDetailsDTO>> searchImportDetails(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ImportDetails for query {}", query);
        Page<ImportDetailsDTO> page = importDetailsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
