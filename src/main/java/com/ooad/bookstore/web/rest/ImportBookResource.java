package com.ooad.bookstore.web.rest;

import com.ooad.bookstore.service.ImportBookService;
import com.ooad.bookstore.web.rest.errors.BadRequestAlertException;
import com.ooad.bookstore.service.dto.ImportBookDTO;
import com.ooad.bookstore.service.dto.ImportBookCriteria;
import com.ooad.bookstore.service.ImportBookQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.ooad.bookstore.domain.ImportBook}.
 */
@RestController
@RequestMapping("/api")
public class ImportBookResource {

    private final Logger log = LoggerFactory.getLogger(ImportBookResource.class);

    private static final String ENTITY_NAME = "importBook";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ImportBookService importBookService;

    private final ImportBookQueryService importBookQueryService;

    public ImportBookResource(ImportBookService importBookService, ImportBookQueryService importBookQueryService) {
        this.importBookService = importBookService;
        this.importBookQueryService = importBookQueryService;
    }

    /**
     * {@code POST  /import-books} : Create a new importBook.
     *
     * @param importBookDTO the importBookDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new importBookDTO, or with status {@code 400 (Bad Request)} if the importBook has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/import-books")
    public ResponseEntity<ImportBookDTO> createImportBook(@RequestBody ImportBookDTO importBookDTO) throws URISyntaxException {
        log.debug("REST request to save ImportBook : {}", importBookDTO);
        if (importBookDTO.getId() != null) {
            throw new BadRequestAlertException("A new importBook cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ImportBookDTO result = importBookService.save(importBookDTO);
        return ResponseEntity.created(new URI("/api/import-books/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /import-books} : Updates an existing importBook.
     *
     * @param importBookDTO the importBookDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated importBookDTO,
     * or with status {@code 400 (Bad Request)} if the importBookDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the importBookDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/import-books")
    public ResponseEntity<ImportBookDTO> updateImportBook(@RequestBody ImportBookDTO importBookDTO) throws URISyntaxException {
        log.debug("REST request to update ImportBook : {}", importBookDTO);
        if (importBookDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ImportBookDTO result = importBookService.save(importBookDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, importBookDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /import-books} : get all the importBooks.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of importBooks in body.
     */
    @GetMapping("/import-books")
    public ResponseEntity<List<ImportBookDTO>> getAllImportBooks(ImportBookCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ImportBooks by criteria: {}", criteria);
        Page<ImportBookDTO> page = importBookQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /import-books/count} : count all the importBooks.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/import-books/count")
    public ResponseEntity<Long> countImportBooks(ImportBookCriteria criteria) {
        log.debug("REST request to count ImportBooks by criteria: {}", criteria);
        return ResponseEntity.ok().body(importBookQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /import-books/:id} : get the "id" importBook.
     *
     * @param id the id of the importBookDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the importBookDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/import-books/{id}")
    public ResponseEntity<ImportBookDTO> getImportBook(@PathVariable Long id) {
        log.debug("REST request to get ImportBook : {}", id);
        Optional<ImportBookDTO> importBookDTO = importBookService.findOne(id);
        return ResponseUtil.wrapOrNotFound(importBookDTO);
    }

    /**
     * {@code DELETE  /import-books/:id} : delete the "id" importBook.
     *
     * @param id the id of the importBookDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/import-books/{id}")
    public ResponseEntity<Void> deleteImportBook(@PathVariable Long id) {
        log.debug("REST request to delete ImportBook : {}", id);
        importBookService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/import-books?query=:query} : search for the importBook corresponding
     * to the query.
     *
     * @param query the query of the importBook search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/import-books")
    public ResponseEntity<List<ImportBookDTO>> searchImportBooks(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ImportBooks for query {}", query);
        Page<ImportBookDTO> page = importBookService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
