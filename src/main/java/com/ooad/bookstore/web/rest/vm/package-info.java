/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ooad.bookstore.web.rest.vm;
