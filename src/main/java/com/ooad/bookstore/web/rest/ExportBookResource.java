package com.ooad.bookstore.web.rest;

import com.ooad.bookstore.service.ExportBookService;
import com.ooad.bookstore.web.rest.errors.BadRequestAlertException;
import com.ooad.bookstore.service.dto.ExportBookDTO;
import com.ooad.bookstore.service.dto.ExportBookCriteria;
import com.ooad.bookstore.service.ExportBookQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.ooad.bookstore.domain.ExportBook}.
 */
@RestController
@RequestMapping("/api")
public class ExportBookResource {

    private final Logger log = LoggerFactory.getLogger(ExportBookResource.class);

    private static final String ENTITY_NAME = "exportBook";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExportBookService exportBookService;

    private final ExportBookQueryService exportBookQueryService;

    public ExportBookResource(ExportBookService exportBookService, ExportBookQueryService exportBookQueryService) {
        this.exportBookService = exportBookService;
        this.exportBookQueryService = exportBookQueryService;
    }

    /**
     * {@code POST  /export-books} : Create a new exportBook.
     *
     * @param exportBookDTO the exportBookDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new exportBookDTO, or with status {@code 400 (Bad Request)} if the exportBook has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/export-books")
    public ResponseEntity<ExportBookDTO> createExportBook(@RequestBody ExportBookDTO exportBookDTO) throws URISyntaxException {
        log.debug("REST request to save ExportBook : {}", exportBookDTO);
        if (exportBookDTO.getId() != null) {
            throw new BadRequestAlertException("A new exportBook cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExportBookDTO result = exportBookService.save(exportBookDTO);
        return ResponseEntity.created(new URI("/api/export-books/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /export-books} : Updates an existing exportBook.
     *
     * @param exportBookDTO the exportBookDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exportBookDTO,
     * or with status {@code 400 (Bad Request)} if the exportBookDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the exportBookDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/export-books")
    public ResponseEntity<ExportBookDTO> updateExportBook(@RequestBody ExportBookDTO exportBookDTO) throws URISyntaxException {
        log.debug("REST request to update ExportBook : {}", exportBookDTO);
        if (exportBookDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ExportBookDTO result = exportBookService.save(exportBookDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exportBookDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /export-books} : get all the exportBooks.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of exportBooks in body.
     */
    @GetMapping("/export-books")
    public ResponseEntity<List<ExportBookDTO>> getAllExportBooks(ExportBookCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ExportBooks by criteria: {}", criteria);
        Page<ExportBookDTO> page = exportBookQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /export-books/count} : count all the exportBooks.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/export-books/count")
    public ResponseEntity<Long> countExportBooks(ExportBookCriteria criteria) {
        log.debug("REST request to count ExportBooks by criteria: {}", criteria);
        return ResponseEntity.ok().body(exportBookQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /export-books/:id} : get the "id" exportBook.
     *
     * @param id the id of the exportBookDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the exportBookDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/export-books/{id}")
    public ResponseEntity<ExportBookDTO> getExportBook(@PathVariable Long id) {
        log.debug("REST request to get ExportBook : {}", id);
        Optional<ExportBookDTO> exportBookDTO = exportBookService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exportBookDTO);
    }

    /**
     * {@code DELETE  /export-books/:id} : delete the "id" exportBook.
     *
     * @param id the id of the exportBookDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/export-books/{id}")
    public ResponseEntity<Void> deleteExportBook(@PathVariable Long id) {
        log.debug("REST request to delete ExportBook : {}", id);
        exportBookService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/export-books?query=:query} : search for the exportBook corresponding
     * to the query.
     *
     * @param query the query of the exportBook search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/export-books")
    public ResponseEntity<List<ExportBookDTO>> searchExportBooks(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ExportBooks for query {}", query);
        Page<ExportBookDTO> page = exportBookService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
