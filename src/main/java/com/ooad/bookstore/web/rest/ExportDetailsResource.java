package com.ooad.bookstore.web.rest;

import com.ooad.bookstore.service.ExportDetailsService;
import com.ooad.bookstore.web.rest.errors.BadRequestAlertException;
import com.ooad.bookstore.service.dto.ExportDetailsDTO;
import com.ooad.bookstore.service.dto.ExportDetailsCriteria;
import com.ooad.bookstore.service.ExportDetailsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.ooad.bookstore.domain.ExportDetails}.
 */
@RestController
@RequestMapping("/api")
public class ExportDetailsResource {

    private final Logger log = LoggerFactory.getLogger(ExportDetailsResource.class);

    private static final String ENTITY_NAME = "exportDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExportDetailsService exportDetailsService;

    private final ExportDetailsQueryService exportDetailsQueryService;

    public ExportDetailsResource(ExportDetailsService exportDetailsService, ExportDetailsQueryService exportDetailsQueryService) {
        this.exportDetailsService = exportDetailsService;
        this.exportDetailsQueryService = exportDetailsQueryService;
    }

    /**
     * {@code POST  /export-details} : Create a new exportDetails.
     *
     * @param exportDetailsDTO the exportDetailsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new exportDetailsDTO, or with status {@code 400 (Bad Request)} if the exportDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/export-details")
    public ResponseEntity<ExportDetailsDTO> createExportDetails(@RequestBody ExportDetailsDTO exportDetailsDTO) throws URISyntaxException {
        log.debug("REST request to save ExportDetails : {}", exportDetailsDTO);
        if (exportDetailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new exportDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExportDetailsDTO result = exportDetailsService.save(exportDetailsDTO);
        return ResponseEntity.created(new URI("/api/export-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /export-details} : Updates an existing exportDetails.
     *
     * @param exportDetailsDTO the exportDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exportDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the exportDetailsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the exportDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/export-details")
    public ResponseEntity<ExportDetailsDTO> updateExportDetails(@RequestBody ExportDetailsDTO exportDetailsDTO) throws URISyntaxException {
        log.debug("REST request to update ExportDetails : {}", exportDetailsDTO);
        if (exportDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ExportDetailsDTO result = exportDetailsService.save(exportDetailsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exportDetailsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /export-details} : get all the exportDetails.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of exportDetails in body.
     */
    @GetMapping("/export-details")
    public ResponseEntity<List<ExportDetailsDTO>> getAllExportDetails(ExportDetailsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ExportDetails by criteria: {}", criteria);
        Page<ExportDetailsDTO> page = exportDetailsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /export-details/count} : count all the exportDetails.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/export-details/count")
    public ResponseEntity<Long> countExportDetails(ExportDetailsCriteria criteria) {
        log.debug("REST request to count ExportDetails by criteria: {}", criteria);
        return ResponseEntity.ok().body(exportDetailsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /export-details/:id} : get the "id" exportDetails.
     *
     * @param id the id of the exportDetailsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the exportDetailsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/export-details/{id}")
    public ResponseEntity<ExportDetailsDTO> getExportDetails(@PathVariable Long id) {
        log.debug("REST request to get ExportDetails : {}", id);
        Optional<ExportDetailsDTO> exportDetailsDTO = exportDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exportDetailsDTO);
    }

    /**
     * {@code DELETE  /export-details/:id} : delete the "id" exportDetails.
     *
     * @param id the id of the exportDetailsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/export-details/{id}")
    public ResponseEntity<Void> deleteExportDetails(@PathVariable Long id) {
        log.debug("REST request to delete ExportDetails : {}", id);
        exportDetailsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/export-details?query=:query} : search for the exportDetails corresponding
     * to the query.
     *
     * @param query the query of the exportDetails search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/export-details")
    public ResponseEntity<List<ExportDetailsDTO>> searchExportDetails(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ExportDetails for query {}", query);
        Page<ExportDetailsDTO> page = exportDetailsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
