package com.ooad.bookstore.service;

import com.ooad.bookstore.service.dto.ImportBookDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.ooad.bookstore.domain.ImportBook}.
 */
public interface ImportBookService {

    /**
     * Save a importBook.
     *
     * @param importBookDTO the entity to save.
     * @return the persisted entity.
     */
    ImportBookDTO save(ImportBookDTO importBookDTO);

    /**
     * Get all the importBooks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ImportBookDTO> findAll(Pageable pageable);

    /**
     * Get the "id" importBook.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ImportBookDTO> findOne(Long id);

    /**
     * Delete the "id" importBook.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the importBook corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ImportBookDTO> search(String query, Pageable pageable);
}
