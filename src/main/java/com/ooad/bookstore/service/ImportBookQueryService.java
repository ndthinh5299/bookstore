package com.ooad.bookstore.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.ooad.bookstore.domain.ImportBook;
import com.ooad.bookstore.domain.*; // for static metamodels
import com.ooad.bookstore.repository.ImportBookRepository;
import com.ooad.bookstore.repository.search.ImportBookSearchRepository;
import com.ooad.bookstore.service.dto.ImportBookCriteria;
import com.ooad.bookstore.service.dto.ImportBookDTO;
import com.ooad.bookstore.service.mapper.ImportBookMapper;

/**
 * Service for executing complex queries for {@link ImportBook} entities in the database.
 * The main input is a {@link ImportBookCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ImportBookDTO} or a {@link Page} of {@link ImportBookDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ImportBookQueryService extends QueryService<ImportBook> {

    private final Logger log = LoggerFactory.getLogger(ImportBookQueryService.class);

    private final ImportBookRepository importBookRepository;

    private final ImportBookMapper importBookMapper;

    private final ImportBookSearchRepository importBookSearchRepository;

    public ImportBookQueryService(ImportBookRepository importBookRepository, ImportBookMapper importBookMapper, ImportBookSearchRepository importBookSearchRepository) {
        this.importBookRepository = importBookRepository;
        this.importBookMapper = importBookMapper;
        this.importBookSearchRepository = importBookSearchRepository;
    }

    /**
     * Return a {@link List} of {@link ImportBookDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ImportBookDTO> findByCriteria(ImportBookCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ImportBook> specification = createSpecification(criteria);
        return importBookMapper.toDto(importBookRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ImportBookDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ImportBookDTO> findByCriteria(ImportBookCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ImportBook> specification = createSpecification(criteria);
        return importBookRepository.findAll(specification, page)
            .map(importBookMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ImportBookCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ImportBook> specification = createSpecification(criteria);
        return importBookRepository.count(specification);
    }

    /**
     * Function to convert {@link ImportBookCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ImportBook> createSpecification(ImportBookCriteria criteria) {
        Specification<ImportBook> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ImportBook_.id));
            }
            if (criteria.getImportDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getImportDate(), ImportBook_.importDate));
            }
            if (criteria.getProvider() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvider(), ImportBook_.provider));
            }
            if (criteria.getEmployeeId() != null) {
                specification = specification.and(buildSpecification(criteria.getEmployeeId(),
                    root -> root.join(ImportBook_.employee, JoinType.LEFT).get(Employee_.id)));
            }
            if (criteria.getImportDetailsId() != null) {
                specification = specification.and(buildSpecification(criteria.getImportDetailsId(),
                    root -> root.join(ImportBook_.importDetails, JoinType.LEFT).get(ImportDetails_.id)));
            }
        }
        return specification;
    }
}
