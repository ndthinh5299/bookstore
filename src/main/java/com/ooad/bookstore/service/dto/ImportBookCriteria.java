package com.ooad.bookstore.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.ooad.bookstore.domain.ImportBook} entity. This class is used
 * in {@link com.ooad.bookstore.web.rest.ImportBookResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /import-books?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ImportBookCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter importDate;

    private StringFilter provider;

    private LongFilter employeeId;

    private LongFilter importDetailsId;

    public ImportBookCriteria() {
    }

    public ImportBookCriteria(ImportBookCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.importDate = other.importDate == null ? null : other.importDate.copy();
        this.provider = other.provider == null ? null : other.provider.copy();
        this.employeeId = other.employeeId == null ? null : other.employeeId.copy();
        this.importDetailsId = other.importDetailsId == null ? null : other.importDetailsId.copy();
    }

    @Override
    public ImportBookCriteria copy() {
        return new ImportBookCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getImportDate() {
        return importDate;
    }

    public void setImportDate(InstantFilter importDate) {
        this.importDate = importDate;
    }

    public StringFilter getProvider() {
        return provider;
    }

    public void setProvider(StringFilter provider) {
        this.provider = provider;
    }

    public LongFilter getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(LongFilter employeeId) {
        this.employeeId = employeeId;
    }

    public LongFilter getImportDetailsId() {
        return importDetailsId;
    }

    public void setImportDetailsId(LongFilter importDetailsId) {
        this.importDetailsId = importDetailsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ImportBookCriteria that = (ImportBookCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(importDate, that.importDate) &&
            Objects.equals(provider, that.provider) &&
            Objects.equals(employeeId, that.employeeId) &&
            Objects.equals(importDetailsId, that.importDetailsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        importDate,
        provider,
        employeeId,
        importDetailsId
        );
    }

    @Override
    public String toString() {
        return "ImportBookCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (importDate != null ? "importDate=" + importDate + ", " : "") +
                (provider != null ? "provider=" + provider + ", " : "") +
                (employeeId != null ? "employeeId=" + employeeId + ", " : "") +
                (importDetailsId != null ? "importDetailsId=" + importDetailsId + ", " : "") +
            "}";
    }

}
