package com.ooad.bookstore.service.dto;

import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.ooad.bookstore.domain.ImportBook} entity.
 */
public class ImportBookDTO implements Serializable {
    
    private Long id;

    private Instant importDate;

    private String provider;


    private Long employeeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getImportDate() {
        return importDate;
    }

    public void setImportDate(Instant importDate) {
        this.importDate = importDate;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ImportBookDTO importBookDTO = (ImportBookDTO) o;
        if (importBookDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), importBookDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ImportBookDTO{" +
            "id=" + getId() +
            ", importDate='" + getImportDate() + "'" +
            ", provider='" + getProvider() + "'" +
            ", employeeId=" + getEmployeeId() +
            "}";
    }
}
