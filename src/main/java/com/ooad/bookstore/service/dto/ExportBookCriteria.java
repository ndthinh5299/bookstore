package com.ooad.bookstore.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.ooad.bookstore.domain.ExportBook} entity. This class is used
 * in {@link com.ooad.bookstore.web.rest.ExportBookResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /export-books?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ExportBookCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter exportDate;

    private FloatFilter totalAmount;

    private LongFilter employeeId;

    private LongFilter customerId;

    private LongFilter importDetailsId;

    public ExportBookCriteria() {
    }

    public ExportBookCriteria(ExportBookCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.exportDate = other.exportDate == null ? null : other.exportDate.copy();
        this.totalAmount = other.totalAmount == null ? null : other.totalAmount.copy();
        this.employeeId = other.employeeId == null ? null : other.employeeId.copy();
        this.customerId = other.customerId == null ? null : other.customerId.copy();
        this.importDetailsId = other.importDetailsId == null ? null : other.importDetailsId.copy();
    }

    @Override
    public ExportBookCriteria copy() {
        return new ExportBookCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getExportDate() {
        return exportDate;
    }

    public void setExportDate(InstantFilter exportDate) {
        this.exportDate = exportDate;
    }

    public FloatFilter getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(FloatFilter totalAmount) {
        this.totalAmount = totalAmount;
    }

    public LongFilter getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(LongFilter employeeId) {
        this.employeeId = employeeId;
    }

    public LongFilter getCustomerId() {
        return customerId;
    }

    public void setCustomerId(LongFilter customerId) {
        this.customerId = customerId;
    }

    public LongFilter getImportDetailsId() {
        return importDetailsId;
    }

    public void setImportDetailsId(LongFilter importDetailsId) {
        this.importDetailsId = importDetailsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ExportBookCriteria that = (ExportBookCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(exportDate, that.exportDate) &&
            Objects.equals(totalAmount, that.totalAmount) &&
            Objects.equals(employeeId, that.employeeId) &&
            Objects.equals(customerId, that.customerId) &&
            Objects.equals(importDetailsId, that.importDetailsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        exportDate,
        totalAmount,
        employeeId,
        customerId,
        importDetailsId
        );
    }

    @Override
    public String toString() {
        return "ExportBookCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (exportDate != null ? "exportDate=" + exportDate + ", " : "") +
                (totalAmount != null ? "totalAmount=" + totalAmount + ", " : "") +
                (employeeId != null ? "employeeId=" + employeeId + ", " : "") +
                (customerId != null ? "customerId=" + customerId + ", " : "") +
                (importDetailsId != null ? "importDetailsId=" + importDetailsId + ", " : "") +
            "}";
    }

}
