package com.ooad.bookstore.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.ooad.bookstore.domain.ImportDetails} entity. This class is used
 * in {@link com.ooad.bookstore.web.rest.ImportDetailsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /import-details?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ImportDetailsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter amount;

    private FloatFilter importPrice;

    private LongFilter bookId;

    private LongFilter importBookId;

    public ImportDetailsCriteria() {
    }

    public ImportDetailsCriteria(ImportDetailsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.importPrice = other.importPrice == null ? null : other.importPrice.copy();
        this.bookId = other.bookId == null ? null : other.bookId.copy();
        this.importBookId = other.importBookId == null ? null : other.importBookId.copy();
    }

    @Override
    public ImportDetailsCriteria copy() {
        return new ImportDetailsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getAmount() {
        return amount;
    }

    public void setAmount(IntegerFilter amount) {
        this.amount = amount;
    }

    public FloatFilter getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(FloatFilter importPrice) {
        this.importPrice = importPrice;
    }

    public LongFilter getBookId() {
        return bookId;
    }

    public void setBookId(LongFilter bookId) {
        this.bookId = bookId;
    }

    public LongFilter getImportBookId() {
        return importBookId;
    }

    public void setImportBookId(LongFilter importBookId) {
        this.importBookId = importBookId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ImportDetailsCriteria that = (ImportDetailsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(importPrice, that.importPrice) &&
            Objects.equals(bookId, that.bookId) &&
            Objects.equals(importBookId, that.importBookId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        amount,
        importPrice,
        bookId,
        importBookId
        );
    }

    @Override
    public String toString() {
        return "ImportDetailsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (importPrice != null ? "importPrice=" + importPrice + ", " : "") +
                (bookId != null ? "bookId=" + bookId + ", " : "") +
                (importBookId != null ? "importBookId=" + importBookId + ", " : "") +
            "}";
    }

}
