package com.ooad.bookstore.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.ooad.bookstore.domain.ExportDetails} entity.
 */
public class ExportDetailsDTO implements Serializable {
    
    private Long id;

    private Integer amount;

    private Float importPrice;


    private Long bookId;

    private Long exportBookId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Float getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(Float importPrice) {
        this.importPrice = importPrice;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getExportBookId() {
        return exportBookId;
    }

    public void setExportBookId(Long exportBookId) {
        this.exportBookId = exportBookId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExportDetailsDTO exportDetailsDTO = (ExportDetailsDTO) o;
        if (exportDetailsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), exportDetailsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ExportDetailsDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", importPrice=" + getImportPrice() +
            ", bookId=" + getBookId() +
            ", exportBookId=" + getExportBookId() +
            "}";
    }
}
