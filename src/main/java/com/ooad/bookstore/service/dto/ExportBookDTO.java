package com.ooad.bookstore.service.dto;

import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.ooad.bookstore.domain.ExportBook} entity.
 */
public class ExportBookDTO implements Serializable {
    
    private Long id;

    private Instant exportDate;

    private Float totalAmount;


    private Long employeeId;

    private Long customerId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getExportDate() {
        return exportDate;
    }

    public void setExportDate(Instant exportDate) {
        this.exportDate = exportDate;
    }

    public Float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExportBookDTO exportBookDTO = (ExportBookDTO) o;
        if (exportBookDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), exportBookDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ExportBookDTO{" +
            "id=" + getId() +
            ", exportDate='" + getExportDate() + "'" +
            ", totalAmount=" + getTotalAmount() +
            ", employeeId=" + getEmployeeId() +
            ", customerId=" + getCustomerId() +
            "}";
    }
}
