package com.ooad.bookstore.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.ooad.bookstore.domain.ExportDetails} entity. This class is used
 * in {@link com.ooad.bookstore.web.rest.ExportDetailsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /export-details?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ExportDetailsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter amount;

    private FloatFilter importPrice;

    private LongFilter bookId;

    private LongFilter exportBookId;

    public ExportDetailsCriteria() {
    }

    public ExportDetailsCriteria(ExportDetailsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.importPrice = other.importPrice == null ? null : other.importPrice.copy();
        this.bookId = other.bookId == null ? null : other.bookId.copy();
        this.exportBookId = other.exportBookId == null ? null : other.exportBookId.copy();
    }

    @Override
    public ExportDetailsCriteria copy() {
        return new ExportDetailsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getAmount() {
        return amount;
    }

    public void setAmount(IntegerFilter amount) {
        this.amount = amount;
    }

    public FloatFilter getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(FloatFilter importPrice) {
        this.importPrice = importPrice;
    }

    public LongFilter getBookId() {
        return bookId;
    }

    public void setBookId(LongFilter bookId) {
        this.bookId = bookId;
    }

    public LongFilter getExportBookId() {
        return exportBookId;
    }

    public void setExportBookId(LongFilter exportBookId) {
        this.exportBookId = exportBookId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ExportDetailsCriteria that = (ExportDetailsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(importPrice, that.importPrice) &&
            Objects.equals(bookId, that.bookId) &&
            Objects.equals(exportBookId, that.exportBookId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        amount,
        importPrice,
        bookId,
        exportBookId
        );
    }

    @Override
    public String toString() {
        return "ExportDetailsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (importPrice != null ? "importPrice=" + importPrice + ", " : "") +
                (bookId != null ? "bookId=" + bookId + ", " : "") +
                (exportBookId != null ? "exportBookId=" + exportBookId + ", " : "") +
            "}";
    }

}
