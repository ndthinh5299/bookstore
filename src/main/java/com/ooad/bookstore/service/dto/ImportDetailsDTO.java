package com.ooad.bookstore.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.ooad.bookstore.domain.ImportDetails} entity.
 */
public class ImportDetailsDTO implements Serializable {
    
    private Long id;

    private Integer amount;

    private Float importPrice;


    private Long bookId;

    private Long importBookId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Float getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(Float importPrice) {
        this.importPrice = importPrice;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getImportBookId() {
        return importBookId;
    }

    public void setImportBookId(Long importBookId) {
        this.importBookId = importBookId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ImportDetailsDTO importDetailsDTO = (ImportDetailsDTO) o;
        if (importDetailsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), importDetailsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ImportDetailsDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", importPrice=" + getImportPrice() +
            ", bookId=" + getBookId() +
            ", importBookId=" + getImportBookId() +
            "}";
    }
}
