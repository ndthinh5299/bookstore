package com.ooad.bookstore.service;

import com.ooad.bookstore.service.dto.ImportDetailsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.ooad.bookstore.domain.ImportDetails}.
 */
public interface ImportDetailsService {

    /**
     * Save a importDetails.
     *
     * @param importDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    ImportDetailsDTO save(ImportDetailsDTO importDetailsDTO);

    /**
     * Get all the importDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ImportDetailsDTO> findAll(Pageable pageable);

    /**
     * Get the "id" importDetails.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ImportDetailsDTO> findOne(Long id);

    /**
     * Delete the "id" importDetails.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the importDetails corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ImportDetailsDTO> search(String query, Pageable pageable);
}
