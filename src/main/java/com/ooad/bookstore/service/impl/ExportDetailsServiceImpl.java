package com.ooad.bookstore.service.impl;

import com.ooad.bookstore.service.ExportDetailsService;
import com.ooad.bookstore.domain.ExportDetails;
import com.ooad.bookstore.repository.ExportDetailsRepository;
import com.ooad.bookstore.repository.search.ExportDetailsSearchRepository;
import com.ooad.bookstore.service.dto.ExportDetailsDTO;
import com.ooad.bookstore.service.mapper.ExportDetailsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link ExportDetails}.
 */
@Service
@Transactional
public class ExportDetailsServiceImpl implements ExportDetailsService {

    private final Logger log = LoggerFactory.getLogger(ExportDetailsServiceImpl.class);

    private final ExportDetailsRepository exportDetailsRepository;

    private final ExportDetailsMapper exportDetailsMapper;

    private final ExportDetailsSearchRepository exportDetailsSearchRepository;

    public ExportDetailsServiceImpl(ExportDetailsRepository exportDetailsRepository, ExportDetailsMapper exportDetailsMapper, ExportDetailsSearchRepository exportDetailsSearchRepository) {
        this.exportDetailsRepository = exportDetailsRepository;
        this.exportDetailsMapper = exportDetailsMapper;
        this.exportDetailsSearchRepository = exportDetailsSearchRepository;
    }

    /**
     * Save a exportDetails.
     *
     * @param exportDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ExportDetailsDTO save(ExportDetailsDTO exportDetailsDTO) {
        log.debug("Request to save ExportDetails : {}", exportDetailsDTO);
        ExportDetails exportDetails = exportDetailsMapper.toEntity(exportDetailsDTO);
        exportDetails = exportDetailsRepository.save(exportDetails);
        ExportDetailsDTO result = exportDetailsMapper.toDto(exportDetails);
        exportDetailsSearchRepository.save(exportDetails);
        return result;
    }

    /**
     * Get all the exportDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ExportDetailsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExportDetails");
        return exportDetailsRepository.findAll(pageable)
            .map(exportDetailsMapper::toDto);
    }

    /**
     * Get one exportDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ExportDetailsDTO> findOne(Long id) {
        log.debug("Request to get ExportDetails : {}", id);
        return exportDetailsRepository.findById(id)
            .map(exportDetailsMapper::toDto);
    }

    /**
     * Delete the exportDetails by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExportDetails : {}", id);
        exportDetailsRepository.deleteById(id);
        exportDetailsSearchRepository.deleteById(id);
    }

    /**
     * Search for the exportDetails corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ExportDetailsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ExportDetails for query {}", query);
        return exportDetailsSearchRepository.search(queryStringQuery(query), pageable)
            .map(exportDetailsMapper::toDto);
    }
}
