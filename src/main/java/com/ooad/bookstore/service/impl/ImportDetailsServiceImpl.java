package com.ooad.bookstore.service.impl;

import com.ooad.bookstore.service.ImportDetailsService;
import com.ooad.bookstore.domain.ImportDetails;
import com.ooad.bookstore.repository.ImportDetailsRepository;
import com.ooad.bookstore.repository.search.ImportDetailsSearchRepository;
import com.ooad.bookstore.service.dto.ImportDetailsDTO;
import com.ooad.bookstore.service.mapper.ImportDetailsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link ImportDetails}.
 */
@Service
@Transactional
public class ImportDetailsServiceImpl implements ImportDetailsService {

    private final Logger log = LoggerFactory.getLogger(ImportDetailsServiceImpl.class);

    private final ImportDetailsRepository importDetailsRepository;

    private final ImportDetailsMapper importDetailsMapper;

    private final ImportDetailsSearchRepository importDetailsSearchRepository;

    public ImportDetailsServiceImpl(ImportDetailsRepository importDetailsRepository, ImportDetailsMapper importDetailsMapper, ImportDetailsSearchRepository importDetailsSearchRepository) {
        this.importDetailsRepository = importDetailsRepository;
        this.importDetailsMapper = importDetailsMapper;
        this.importDetailsSearchRepository = importDetailsSearchRepository;
    }

    /**
     * Save a importDetails.
     *
     * @param importDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ImportDetailsDTO save(ImportDetailsDTO importDetailsDTO) {
        log.debug("Request to save ImportDetails : {}", importDetailsDTO);
        ImportDetails importDetails = importDetailsMapper.toEntity(importDetailsDTO);
        importDetails = importDetailsRepository.save(importDetails);
        ImportDetailsDTO result = importDetailsMapper.toDto(importDetails);
        importDetailsSearchRepository.save(importDetails);
        return result;
    }

    /**
     * Get all the importDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ImportDetailsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ImportDetails");
        return importDetailsRepository.findAll(pageable)
            .map(importDetailsMapper::toDto);
    }

    /**
     * Get one importDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ImportDetailsDTO> findOne(Long id) {
        log.debug("Request to get ImportDetails : {}", id);
        return importDetailsRepository.findById(id)
            .map(importDetailsMapper::toDto);
    }

    /**
     * Delete the importDetails by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ImportDetails : {}", id);
        importDetailsRepository.deleteById(id);
        importDetailsSearchRepository.deleteById(id);
    }

    /**
     * Search for the importDetails corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ImportDetailsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ImportDetails for query {}", query);
        return importDetailsSearchRepository.search(queryStringQuery(query), pageable)
            .map(importDetailsMapper::toDto);
    }
}
