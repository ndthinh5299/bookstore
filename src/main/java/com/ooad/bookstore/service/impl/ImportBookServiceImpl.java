package com.ooad.bookstore.service.impl;

import com.ooad.bookstore.service.ImportBookService;
import com.ooad.bookstore.domain.ImportBook;
import com.ooad.bookstore.repository.ImportBookRepository;
import com.ooad.bookstore.repository.search.ImportBookSearchRepository;
import com.ooad.bookstore.service.dto.ImportBookDTO;
import com.ooad.bookstore.service.mapper.ImportBookMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link ImportBook}.
 */
@Service
@Transactional
public class ImportBookServiceImpl implements ImportBookService {

    private final Logger log = LoggerFactory.getLogger(ImportBookServiceImpl.class);

    private final ImportBookRepository importBookRepository;

    private final ImportBookMapper importBookMapper;

    private final ImportBookSearchRepository importBookSearchRepository;

    public ImportBookServiceImpl(ImportBookRepository importBookRepository, ImportBookMapper importBookMapper, ImportBookSearchRepository importBookSearchRepository) {
        this.importBookRepository = importBookRepository;
        this.importBookMapper = importBookMapper;
        this.importBookSearchRepository = importBookSearchRepository;
    }

    /**
     * Save a importBook.
     *
     * @param importBookDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ImportBookDTO save(ImportBookDTO importBookDTO) {
        log.debug("Request to save ImportBook : {}", importBookDTO);
        ImportBook importBook = importBookMapper.toEntity(importBookDTO);
        importBook = importBookRepository.save(importBook);
        ImportBookDTO result = importBookMapper.toDto(importBook);
        importBookSearchRepository.save(importBook);
        return result;
    }

    /**
     * Get all the importBooks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ImportBookDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ImportBooks");
        return importBookRepository.findAll(pageable)
            .map(importBookMapper::toDto);
    }

    /**
     * Get one importBook by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ImportBookDTO> findOne(Long id) {
        log.debug("Request to get ImportBook : {}", id);
        return importBookRepository.findById(id)
            .map(importBookMapper::toDto);
    }

    /**
     * Delete the importBook by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ImportBook : {}", id);
        importBookRepository.deleteById(id);
        importBookSearchRepository.deleteById(id);
    }

    /**
     * Search for the importBook corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ImportBookDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ImportBooks for query {}", query);
        return importBookSearchRepository.search(queryStringQuery(query), pageable)
            .map(importBookMapper::toDto);
    }
}
