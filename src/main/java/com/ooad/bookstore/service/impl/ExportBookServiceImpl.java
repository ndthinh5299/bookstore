package com.ooad.bookstore.service.impl;

import com.ooad.bookstore.service.ExportBookService;
import com.ooad.bookstore.domain.ExportBook;
import com.ooad.bookstore.repository.ExportBookRepository;
import com.ooad.bookstore.repository.search.ExportBookSearchRepository;
import com.ooad.bookstore.service.dto.ExportBookDTO;
import com.ooad.bookstore.service.mapper.ExportBookMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link ExportBook}.
 */
@Service
@Transactional
public class ExportBookServiceImpl implements ExportBookService {

    private final Logger log = LoggerFactory.getLogger(ExportBookServiceImpl.class);

    private final ExportBookRepository exportBookRepository;

    private final ExportBookMapper exportBookMapper;

    private final ExportBookSearchRepository exportBookSearchRepository;

    public ExportBookServiceImpl(ExportBookRepository exportBookRepository, ExportBookMapper exportBookMapper, ExportBookSearchRepository exportBookSearchRepository) {
        this.exportBookRepository = exportBookRepository;
        this.exportBookMapper = exportBookMapper;
        this.exportBookSearchRepository = exportBookSearchRepository;
    }

    /**
     * Save a exportBook.
     *
     * @param exportBookDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ExportBookDTO save(ExportBookDTO exportBookDTO) {
        log.debug("Request to save ExportBook : {}", exportBookDTO);
        ExportBook exportBook = exportBookMapper.toEntity(exportBookDTO);
        exportBook = exportBookRepository.save(exportBook);
        ExportBookDTO result = exportBookMapper.toDto(exportBook);
        exportBookSearchRepository.save(exportBook);
        return result;
    }

    /**
     * Get all the exportBooks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ExportBookDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExportBooks");
        return exportBookRepository.findAll(pageable)
            .map(exportBookMapper::toDto);
    }

    /**
     * Get one exportBook by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ExportBookDTO> findOne(Long id) {
        log.debug("Request to get ExportBook : {}", id);
        return exportBookRepository.findById(id)
            .map(exportBookMapper::toDto);
    }

    /**
     * Delete the exportBook by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExportBook : {}", id);
        exportBookRepository.deleteById(id);
        exportBookSearchRepository.deleteById(id);
    }

    /**
     * Search for the exportBook corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ExportBookDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ExportBooks for query {}", query);
        return exportBookSearchRepository.search(queryStringQuery(query), pageable)
            .map(exportBookMapper::toDto);
    }
}
