package com.ooad.bookstore.service;

import com.ooad.bookstore.service.dto.ExportDetailsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.ooad.bookstore.domain.ExportDetails}.
 */
public interface ExportDetailsService {

    /**
     * Save a exportDetails.
     *
     * @param exportDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    ExportDetailsDTO save(ExportDetailsDTO exportDetailsDTO);

    /**
     * Get all the exportDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExportDetailsDTO> findAll(Pageable pageable);

    /**
     * Get the "id" exportDetails.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExportDetailsDTO> findOne(Long id);

    /**
     * Delete the "id" exportDetails.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the exportDetails corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExportDetailsDTO> search(String query, Pageable pageable);
}
