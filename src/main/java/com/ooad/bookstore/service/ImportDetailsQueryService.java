package com.ooad.bookstore.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.ooad.bookstore.domain.ImportDetails;
import com.ooad.bookstore.domain.*; // for static metamodels
import com.ooad.bookstore.repository.ImportDetailsRepository;
import com.ooad.bookstore.repository.search.ImportDetailsSearchRepository;
import com.ooad.bookstore.service.dto.ImportDetailsCriteria;
import com.ooad.bookstore.service.dto.ImportDetailsDTO;
import com.ooad.bookstore.service.mapper.ImportDetailsMapper;

/**
 * Service for executing complex queries for {@link ImportDetails} entities in the database.
 * The main input is a {@link ImportDetailsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ImportDetailsDTO} or a {@link Page} of {@link ImportDetailsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ImportDetailsQueryService extends QueryService<ImportDetails> {

    private final Logger log = LoggerFactory.getLogger(ImportDetailsQueryService.class);

    private final ImportDetailsRepository importDetailsRepository;

    private final ImportDetailsMapper importDetailsMapper;

    private final ImportDetailsSearchRepository importDetailsSearchRepository;

    public ImportDetailsQueryService(ImportDetailsRepository importDetailsRepository, ImportDetailsMapper importDetailsMapper, ImportDetailsSearchRepository importDetailsSearchRepository) {
        this.importDetailsRepository = importDetailsRepository;
        this.importDetailsMapper = importDetailsMapper;
        this.importDetailsSearchRepository = importDetailsSearchRepository;
    }

    /**
     * Return a {@link List} of {@link ImportDetailsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ImportDetailsDTO> findByCriteria(ImportDetailsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ImportDetails> specification = createSpecification(criteria);
        return importDetailsMapper.toDto(importDetailsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ImportDetailsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ImportDetailsDTO> findByCriteria(ImportDetailsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ImportDetails> specification = createSpecification(criteria);
        return importDetailsRepository.findAll(specification, page)
            .map(importDetailsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ImportDetailsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ImportDetails> specification = createSpecification(criteria);
        return importDetailsRepository.count(specification);
    }

    /**
     * Function to convert {@link ImportDetailsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ImportDetails> createSpecification(ImportDetailsCriteria criteria) {
        Specification<ImportDetails> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ImportDetails_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), ImportDetails_.amount));
            }
            if (criteria.getImportPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getImportPrice(), ImportDetails_.importPrice));
            }
            if (criteria.getBookId() != null) {
                specification = specification.and(buildSpecification(criteria.getBookId(),
                    root -> root.join(ImportDetails_.book, JoinType.LEFT).get(Book_.id)));
            }
            if (criteria.getImportBookId() != null) {
                specification = specification.and(buildSpecification(criteria.getImportBookId(),
                    root -> root.join(ImportDetails_.importBook, JoinType.LEFT).get(ImportBook_.id)));
            }
        }
        return specification;
    }
}
