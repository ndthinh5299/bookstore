package com.ooad.bookstore.service.mapper;


import com.ooad.bookstore.domain.*;
import com.ooad.bookstore.service.dto.ImportDetailsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ImportDetails} and its DTO {@link ImportDetailsDTO}.
 */
@Mapper(componentModel = "spring", uses = {BookMapper.class, ImportBookMapper.class})
public interface ImportDetailsMapper extends EntityMapper<ImportDetailsDTO, ImportDetails> {

    @Mapping(source = "book.id", target = "bookId")
    @Mapping(source = "importBook.id", target = "importBookId")
    ImportDetailsDTO toDto(ImportDetails importDetails);

    @Mapping(source = "bookId", target = "book")
    @Mapping(source = "importBookId", target = "importBook")
    ImportDetails toEntity(ImportDetailsDTO importDetailsDTO);

    default ImportDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        ImportDetails importDetails = new ImportDetails();
        importDetails.setId(id);
        return importDetails;
    }
}
