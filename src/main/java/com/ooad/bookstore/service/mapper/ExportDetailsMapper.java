package com.ooad.bookstore.service.mapper;


import com.ooad.bookstore.domain.*;
import com.ooad.bookstore.service.dto.ExportDetailsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExportDetails} and its DTO {@link ExportDetailsDTO}.
 */
@Mapper(componentModel = "spring", uses = {BookMapper.class, ExportBookMapper.class})
public interface ExportDetailsMapper extends EntityMapper<ExportDetailsDTO, ExportDetails> {

    @Mapping(source = "book.id", target = "bookId")
    @Mapping(source = "exportBook.id", target = "exportBookId")
    ExportDetailsDTO toDto(ExportDetails exportDetails);

    @Mapping(source = "bookId", target = "book")
    @Mapping(source = "exportBookId", target = "exportBook")
    ExportDetails toEntity(ExportDetailsDTO exportDetailsDTO);

    default ExportDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        ExportDetails exportDetails = new ExportDetails();
        exportDetails.setId(id);
        return exportDetails;
    }
}
