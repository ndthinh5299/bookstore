package com.ooad.bookstore.service.mapper;


import com.ooad.bookstore.domain.*;
import com.ooad.bookstore.service.dto.ExportBookDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExportBook} and its DTO {@link ExportBookDTO}.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class, CustomerMapper.class})
public interface ExportBookMapper extends EntityMapper<ExportBookDTO, ExportBook> {

    @Mapping(source = "employee.id", target = "employeeId")
    @Mapping(source = "customer.id", target = "customerId")
    ExportBookDTO toDto(ExportBook exportBook);

    @Mapping(source = "employeeId", target = "employee")
    @Mapping(source = "customerId", target = "customer")
    @Mapping(target = "importDetails", ignore = true)
    @Mapping(target = "removeImportDetails", ignore = true)
    ExportBook toEntity(ExportBookDTO exportBookDTO);

    default ExportBook fromId(Long id) {
        if (id == null) {
            return null;
        }
        ExportBook exportBook = new ExportBook();
        exportBook.setId(id);
        return exportBook;
    }
}
