package com.ooad.bookstore.service.mapper;


import com.ooad.bookstore.domain.*;
import com.ooad.bookstore.service.dto.ImportBookDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ImportBook} and its DTO {@link ImportBookDTO}.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface ImportBookMapper extends EntityMapper<ImportBookDTO, ImportBook> {

    @Mapping(source = "employee.id", target = "employeeId")
    ImportBookDTO toDto(ImportBook importBook);

    @Mapping(source = "employeeId", target = "employee")
    @Mapping(target = "importDetails", ignore = true)
    @Mapping(target = "removeImportDetails", ignore = true)
    ImportBook toEntity(ImportBookDTO importBookDTO);

    default ImportBook fromId(Long id) {
        if (id == null) {
            return null;
        }
        ImportBook importBook = new ImportBook();
        importBook.setId(id);
        return importBook;
    }
}
