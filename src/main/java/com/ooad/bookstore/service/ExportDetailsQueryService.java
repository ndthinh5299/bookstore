package com.ooad.bookstore.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.ooad.bookstore.domain.ExportDetails;
import com.ooad.bookstore.domain.*; // for static metamodels
import com.ooad.bookstore.repository.ExportDetailsRepository;
import com.ooad.bookstore.repository.search.ExportDetailsSearchRepository;
import com.ooad.bookstore.service.dto.ExportDetailsCriteria;
import com.ooad.bookstore.service.dto.ExportDetailsDTO;
import com.ooad.bookstore.service.mapper.ExportDetailsMapper;

/**
 * Service for executing complex queries for {@link ExportDetails} entities in the database.
 * The main input is a {@link ExportDetailsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ExportDetailsDTO} or a {@link Page} of {@link ExportDetailsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ExportDetailsQueryService extends QueryService<ExportDetails> {

    private final Logger log = LoggerFactory.getLogger(ExportDetailsQueryService.class);

    private final ExportDetailsRepository exportDetailsRepository;

    private final ExportDetailsMapper exportDetailsMapper;

    private final ExportDetailsSearchRepository exportDetailsSearchRepository;

    public ExportDetailsQueryService(ExportDetailsRepository exportDetailsRepository, ExportDetailsMapper exportDetailsMapper, ExportDetailsSearchRepository exportDetailsSearchRepository) {
        this.exportDetailsRepository = exportDetailsRepository;
        this.exportDetailsMapper = exportDetailsMapper;
        this.exportDetailsSearchRepository = exportDetailsSearchRepository;
    }

    /**
     * Return a {@link List} of {@link ExportDetailsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ExportDetailsDTO> findByCriteria(ExportDetailsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ExportDetails> specification = createSpecification(criteria);
        return exportDetailsMapper.toDto(exportDetailsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ExportDetailsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ExportDetailsDTO> findByCriteria(ExportDetailsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ExportDetails> specification = createSpecification(criteria);
        return exportDetailsRepository.findAll(specification, page)
            .map(exportDetailsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ExportDetailsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ExportDetails> specification = createSpecification(criteria);
        return exportDetailsRepository.count(specification);
    }

    /**
     * Function to convert {@link ExportDetailsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ExportDetails> createSpecification(ExportDetailsCriteria criteria) {
        Specification<ExportDetails> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ExportDetails_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), ExportDetails_.amount));
            }
            if (criteria.getImportPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getImportPrice(), ExportDetails_.importPrice));
            }
            if (criteria.getBookId() != null) {
                specification = specification.and(buildSpecification(criteria.getBookId(),
                    root -> root.join(ExportDetails_.book, JoinType.LEFT).get(Book_.id)));
            }
            if (criteria.getExportBookId() != null) {
                specification = specification.and(buildSpecification(criteria.getExportBookId(),
                    root -> root.join(ExportDetails_.exportBook, JoinType.LEFT).get(ExportBook_.id)));
            }
        }
        return specification;
    }
}
