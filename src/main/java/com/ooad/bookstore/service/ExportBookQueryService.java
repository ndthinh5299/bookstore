package com.ooad.bookstore.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.ooad.bookstore.domain.ExportBook;
import com.ooad.bookstore.domain.*; // for static metamodels
import com.ooad.bookstore.repository.ExportBookRepository;
import com.ooad.bookstore.repository.search.ExportBookSearchRepository;
import com.ooad.bookstore.service.dto.ExportBookCriteria;
import com.ooad.bookstore.service.dto.ExportBookDTO;
import com.ooad.bookstore.service.mapper.ExportBookMapper;

/**
 * Service for executing complex queries for {@link ExportBook} entities in the database.
 * The main input is a {@link ExportBookCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ExportBookDTO} or a {@link Page} of {@link ExportBookDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ExportBookQueryService extends QueryService<ExportBook> {

    private final Logger log = LoggerFactory.getLogger(ExportBookQueryService.class);

    private final ExportBookRepository exportBookRepository;

    private final ExportBookMapper exportBookMapper;

    private final ExportBookSearchRepository exportBookSearchRepository;

    public ExportBookQueryService(ExportBookRepository exportBookRepository, ExportBookMapper exportBookMapper, ExportBookSearchRepository exportBookSearchRepository) {
        this.exportBookRepository = exportBookRepository;
        this.exportBookMapper = exportBookMapper;
        this.exportBookSearchRepository = exportBookSearchRepository;
    }

    /**
     * Return a {@link List} of {@link ExportBookDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ExportBookDTO> findByCriteria(ExportBookCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ExportBook> specification = createSpecification(criteria);
        return exportBookMapper.toDto(exportBookRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ExportBookDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ExportBookDTO> findByCriteria(ExportBookCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ExportBook> specification = createSpecification(criteria);
        return exportBookRepository.findAll(specification, page)
            .map(exportBookMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ExportBookCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ExportBook> specification = createSpecification(criteria);
        return exportBookRepository.count(specification);
    }

    /**
     * Function to convert {@link ExportBookCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ExportBook> createSpecification(ExportBookCriteria criteria) {
        Specification<ExportBook> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ExportBook_.id));
            }
            if (criteria.getExportDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExportDate(), ExportBook_.exportDate));
            }
            if (criteria.getTotalAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalAmount(), ExportBook_.totalAmount));
            }
            if (criteria.getEmployeeId() != null) {
                specification = specification.and(buildSpecification(criteria.getEmployeeId(),
                    root -> root.join(ExportBook_.employee, JoinType.LEFT).get(Employee_.id)));
            }
            if (criteria.getCustomerId() != null) {
                specification = specification.and(buildSpecification(criteria.getCustomerId(),
                    root -> root.join(ExportBook_.customer, JoinType.LEFT).get(Customer_.id)));
            }
            if (criteria.getImportDetailsId() != null) {
                specification = specification.and(buildSpecification(criteria.getImportDetailsId(),
                    root -> root.join(ExportBook_.importDetails, JoinType.LEFT).get(ExportDetails_.id)));
            }
        }
        return specification;
    }
}
