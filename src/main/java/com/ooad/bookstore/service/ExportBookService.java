package com.ooad.bookstore.service;

import com.ooad.bookstore.service.dto.ExportBookDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.ooad.bookstore.domain.ExportBook}.
 */
public interface ExportBookService {

    /**
     * Save a exportBook.
     *
     * @param exportBookDTO the entity to save.
     * @return the persisted entity.
     */
    ExportBookDTO save(ExportBookDTO exportBookDTO);

    /**
     * Get all the exportBooks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExportBookDTO> findAll(Pageable pageable);

    /**
     * Get the "id" exportBook.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExportBookDTO> findOne(Long id);

    /**
     * Delete the "id" exportBook.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the exportBook corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExportBookDTO> search(String query, Pageable pageable);
}
