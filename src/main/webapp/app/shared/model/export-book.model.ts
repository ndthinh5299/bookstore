import { Moment } from 'moment';
import { IExportDetails } from 'app/shared/model/export-details.model';

export interface IExportBook {
  id?: number;
  exportDate?: Moment;
  totalAmount?: number;
  employeeId?: number;
  customerId?: number;
  importDetails?: IExportDetails[];
}

export class ExportBook implements IExportBook {
  constructor(
    public id?: number,
    public exportDate?: Moment,
    public totalAmount?: number,
    public employeeId?: number,
    public customerId?: number,
    public importDetails?: IExportDetails[]
  ) {}
}
