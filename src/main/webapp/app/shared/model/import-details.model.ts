export interface IImportDetails {
  id?: number;
  amount?: number;
  importPrice?: number;
  bookId?: number;
  importBookId?: number;
}

export class ImportDetails implements IImportDetails {
  constructor(
    public id?: number,
    public amount?: number,
    public importPrice?: number,
    public bookId?: number,
    public importBookId?: number
  ) {}
}
