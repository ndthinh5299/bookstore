import { Moment } from 'moment';
import { IImportDetails } from 'app/shared/model/import-details.model';

export interface IImportBook {
  id?: number;
  importDate?: Moment;
  provider?: string;
  employeeId?: number;
  importDetails?: IImportDetails[];
}

export class ImportBook implements IImportBook {
  constructor(
    public id?: number,
    public importDate?: Moment,
    public provider?: string,
    public employeeId?: number,
    public importDetails?: IImportDetails[]
  ) {}
}
