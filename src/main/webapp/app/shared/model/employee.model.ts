export interface IEmployee {
  id?: number;
  salary?: number;
}

export class Employee implements IEmployee {
  constructor(public id?: number, public salary?: number) {}
}
