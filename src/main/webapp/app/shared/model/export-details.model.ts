export interface IExportDetails {
  id?: number;
  amount?: number;
  importPrice?: number;
  bookId?: number;
  exportBookId?: number;
}

export class ExportDetails implements IExportDetails {
  constructor(
    public id?: number,
    public amount?: number,
    public importPrice?: number,
    public bookId?: number,
    public exportBookId?: number
  ) {}
}
