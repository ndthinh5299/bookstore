export interface IBook {
  id?: number;
  name?: string;
  author?: string;
  year?: number;
  publisher?: string;
  amount?: number;
  price?: number;
}

export class Book implements IBook {
  constructor(
    public id?: number,
    public name?: string,
    public author?: string,
    public year?: number,
    public publisher?: string,
    public amount?: number,
    public price?: number
  ) {}
}
