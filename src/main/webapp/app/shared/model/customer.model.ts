export interface ICustomer {
  id?: number;
  name?: string;
  phone?: string;
  address?: string;
}

export class Customer implements ICustomer {
  constructor(public id?: number, public name?: string, public phone?: string, public address?: string) {}
}
