import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { IExportBook } from 'app/shared/model/export-book.model';

type EntityResponseType = HttpResponse<IExportBook>;
type EntityArrayResponseType = HttpResponse<IExportBook[]>;

@Injectable({ providedIn: 'root' })
export class ExportBookService {
  public resourceUrl = SERVER_API_URL + 'api/export-books';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/export-books';

  constructor(protected http: HttpClient) {}

  create(exportBook: IExportBook): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(exportBook);
    return this.http
      .post<IExportBook>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(exportBook: IExportBook): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(exportBook);
    return this.http
      .put<IExportBook>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IExportBook>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExportBook[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExportBook[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(exportBook: IExportBook): IExportBook {
    const copy: IExportBook = Object.assign({}, exportBook, {
      exportDate: exportBook.exportDate && exportBook.exportDate.isValid() ? exportBook.exportDate.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.exportDate = res.body.exportDate ? moment(res.body.exportDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((exportBook: IExportBook) => {
        exportBook.exportDate = exportBook.exportDate ? moment(exportBook.exportDate) : undefined;
      });
    }
    return res;
  }
}
