import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { ExportBookComponent } from './export-book.component';
import { ExportBookDetailComponent } from './export-book-detail.component';
import { ExportBookUpdateComponent } from './export-book-update.component';
import { ExportBookDeleteDialogComponent } from './export-book-delete-dialog.component';
import { exportBookRoute } from './export-book.route';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(exportBookRoute)],
  declarations: [ExportBookComponent, ExportBookDetailComponent, ExportBookUpdateComponent, ExportBookDeleteDialogComponent],
  entryComponents: [ExportBookDeleteDialogComponent]
})
export class ExportBookModule {}
