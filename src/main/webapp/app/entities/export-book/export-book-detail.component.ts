import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExportBook } from 'app/shared/model/export-book.model';

@Component({
  selector: 'jhi-export-book-detail',
  templateUrl: './export-book-detail.component.html'
})
export class ExportBookDetailComponent implements OnInit {
  exportBook: IExportBook | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ exportBook }) => (this.exportBook = exportBook));
  }

  previousState(): void {
    window.history.back();
  }
}
