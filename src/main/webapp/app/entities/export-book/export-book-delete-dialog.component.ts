import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IExportBook } from 'app/shared/model/export-book.model';
import { ExportBookService } from './export-book.service';

@Component({
  templateUrl: './export-book-delete-dialog.component.html'
})
export class ExportBookDeleteDialogComponent {
  exportBook?: IExportBook;

  constructor(
    protected exportBookService: ExportBookService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.exportBookService.delete(id).subscribe(() => {
      this.eventManager.broadcast('exportBookListModification');
      this.activeModal.close();
    });
  }
}
