import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IExportBook, ExportBook } from 'app/shared/model/export-book.model';
import { ExportBookService } from './export-book.service';
import { IEmployee } from 'app/shared/model/employee.model';
import { EmployeeService } from 'app/entities/employee/employee.service';
import { ICustomer } from 'app/shared/model/customer.model';
import { CustomerService } from 'app/entities/customer/customer.service';

type SelectableEntity = IEmployee | ICustomer;

@Component({
  selector: 'jhi-export-book-update',
  templateUrl: './export-book-update.component.html'
})
export class ExportBookUpdateComponent implements OnInit {
  isSaving = false;
  employees: IEmployee[] = [];
  customers: ICustomer[] = [];

  editForm = this.fb.group({
    id: [],
    exportDate: [],
    totalAmount: [],
    employeeId: [],
    customerId: []
  });

  constructor(
    protected exportBookService: ExportBookService,
    protected employeeService: EmployeeService,
    protected customerService: CustomerService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ exportBook }) => {
      if (!exportBook.id) {
        const today = moment().startOf('day');
        exportBook.exportDate = today;
      }

      this.updateForm(exportBook);

      this.employeeService
        .query({ 'exportBookId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IEmployee[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IEmployee[]) => {
          if (!exportBook.employeeId) {
            this.employees = resBody;
          } else {
            this.employeeService
              .find(exportBook.employeeId)
              .pipe(
                map((subRes: HttpResponse<IEmployee>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IEmployee[]) => (this.employees = concatRes));
          }
        });

      this.customerService
        .query({ 'exportBookId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<ICustomer[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: ICustomer[]) => {
          if (!exportBook.customerId) {
            this.customers = resBody;
          } else {
            this.customerService
              .find(exportBook.customerId)
              .pipe(
                map((subRes: HttpResponse<ICustomer>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: ICustomer[]) => (this.customers = concatRes));
          }
        });
    });
  }

  updateForm(exportBook: IExportBook): void {
    this.editForm.patchValue({
      id: exportBook.id,
      exportDate: exportBook.exportDate ? exportBook.exportDate.format(DATE_TIME_FORMAT) : null,
      totalAmount: exportBook.totalAmount,
      employeeId: exportBook.employeeId,
      customerId: exportBook.customerId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const exportBook = this.createFromForm();
    if (exportBook.id !== undefined) {
      this.subscribeToSaveResponse(this.exportBookService.update(exportBook));
    } else {
      this.subscribeToSaveResponse(this.exportBookService.create(exportBook));
    }
  }

  private createFromForm(): IExportBook {
    return {
      ...new ExportBook(),
      id: this.editForm.get(['id'])!.value,
      exportDate: this.editForm.get(['exportDate'])!.value ? moment(this.editForm.get(['exportDate'])!.value, DATE_TIME_FORMAT) : undefined,
      totalAmount: this.editForm.get(['totalAmount'])!.value,
      employeeId: this.editForm.get(['employeeId'])!.value,
      customerId: this.editForm.get(['customerId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExportBook>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
