import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IExportBook, ExportBook } from 'app/shared/model/export-book.model';
import { ExportBookService } from './export-book.service';
import { ExportBookComponent } from './export-book.component';
import { ExportBookDetailComponent } from './export-book-detail.component';
import { ExportBookUpdateComponent } from './export-book-update.component';

@Injectable({ providedIn: 'root' })
export class ExportBookResolve implements Resolve<IExportBook> {
  constructor(private service: ExportBookService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IExportBook> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((exportBook: HttpResponse<ExportBook>) => {
          if (exportBook.body) {
            return of(exportBook.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ExportBook());
  }
}

export const exportBookRoute: Routes = [
  {
    path: '',
    component: ExportBookComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'App.exportBook.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ExportBookDetailComponent,
    resolve: {
      exportBook: ExportBookResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.exportBook.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ExportBookUpdateComponent,
    resolve: {
      exportBook: ExportBookResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.exportBook.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ExportBookUpdateComponent,
    resolve: {
      exportBook: ExportBookResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.exportBook.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
