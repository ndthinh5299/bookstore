import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IImportDetails } from 'app/shared/model/import-details.model';
import { ImportDetailsService } from './import-details.service';

@Component({
  templateUrl: './import-details-delete-dialog.component.html'
})
export class ImportDetailsDeleteDialogComponent {
  importDetails?: IImportDetails;

  constructor(
    protected importDetailsService: ImportDetailsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.importDetailsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('importDetailsListModification');
      this.activeModal.close();
    });
  }
}
