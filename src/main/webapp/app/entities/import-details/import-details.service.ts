import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { IImportDetails } from 'app/shared/model/import-details.model';

type EntityResponseType = HttpResponse<IImportDetails>;
type EntityArrayResponseType = HttpResponse<IImportDetails[]>;

@Injectable({ providedIn: 'root' })
export class ImportDetailsService {
  public resourceUrl = SERVER_API_URL + 'api/import-details';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/import-details';

  constructor(protected http: HttpClient) {}

  create(importDetails: IImportDetails): Observable<EntityResponseType> {
    return this.http.post<IImportDetails>(this.resourceUrl, importDetails, { observe: 'response' });
  }

  update(importDetails: IImportDetails): Observable<EntityResponseType> {
    return this.http.put<IImportDetails>(this.resourceUrl, importDetails, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IImportDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IImportDetails[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IImportDetails[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
