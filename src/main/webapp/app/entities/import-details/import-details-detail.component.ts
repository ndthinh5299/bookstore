import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IImportDetails } from 'app/shared/model/import-details.model';

@Component({
  selector: 'jhi-import-details-detail',
  templateUrl: './import-details-detail.component.html'
})
export class ImportDetailsDetailComponent implements OnInit {
  importDetails: IImportDetails | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ importDetails }) => (this.importDetails = importDetails));
  }

  previousState(): void {
    window.history.back();
  }
}
