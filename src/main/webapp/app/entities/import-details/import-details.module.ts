import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { ImportDetailsComponent } from './import-details.component';
import { ImportDetailsDetailComponent } from './import-details-detail.component';
import { ImportDetailsUpdateComponent } from './import-details-update.component';
import { ImportDetailsDeleteDialogComponent } from './import-details-delete-dialog.component';
import { importDetailsRoute } from './import-details.route';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(importDetailsRoute)],
  declarations: [ImportDetailsComponent, ImportDetailsDetailComponent, ImportDetailsUpdateComponent, ImportDetailsDeleteDialogComponent],
  entryComponents: [ImportDetailsDeleteDialogComponent]
})
export class ImportDetailsModule {}
