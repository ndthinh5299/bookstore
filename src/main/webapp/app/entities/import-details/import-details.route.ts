import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IImportDetails, ImportDetails } from 'app/shared/model/import-details.model';
import { ImportDetailsService } from './import-details.service';
import { ImportDetailsComponent } from './import-details.component';
import { ImportDetailsDetailComponent } from './import-details-detail.component';
import { ImportDetailsUpdateComponent } from './import-details-update.component';

@Injectable({ providedIn: 'root' })
export class ImportDetailsResolve implements Resolve<IImportDetails> {
  constructor(private service: ImportDetailsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IImportDetails> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((importDetails: HttpResponse<ImportDetails>) => {
          if (importDetails.body) {
            return of(importDetails.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ImportDetails());
  }
}

export const importDetailsRoute: Routes = [
  {
    path: '',
    component: ImportDetailsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'App.importDetails.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ImportDetailsDetailComponent,
    resolve: {
      importDetails: ImportDetailsResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.importDetails.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ImportDetailsUpdateComponent,
    resolve: {
      importDetails: ImportDetailsResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.importDetails.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ImportDetailsUpdateComponent,
    resolve: {
      importDetails: ImportDetailsResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.importDetails.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
