import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IImportDetails, ImportDetails } from 'app/shared/model/import-details.model';
import { ImportDetailsService } from './import-details.service';
import { IBook } from 'app/shared/model/book.model';
import { BookService } from 'app/entities/book/book.service';
import { IImportBook } from 'app/shared/model/import-book.model';
import { ImportBookService } from 'app/entities/import-book/import-book.service';

type SelectableEntity = IBook | IImportBook;

@Component({
  selector: 'jhi-import-details-update',
  templateUrl: './import-details-update.component.html'
})
export class ImportDetailsUpdateComponent implements OnInit {
  isSaving = false;
  books: IBook[] = [];
  importbooks: IImportBook[] = [];

  editForm = this.fb.group({
    id: [],
    amount: [],
    importPrice: [],
    bookId: [],
    importBookId: []
  });

  constructor(
    protected importDetailsService: ImportDetailsService,
    protected bookService: BookService,
    protected importBookService: ImportBookService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ importDetails }) => {
      this.updateForm(importDetails);

      this.bookService
        .query({ 'importDetailsId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IBook[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IBook[]) => {
          if (!importDetails.bookId) {
            this.books = resBody;
          } else {
            this.bookService
              .find(importDetails.bookId)
              .pipe(
                map((subRes: HttpResponse<IBook>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IBook[]) => (this.books = concatRes));
          }
        });

      this.importBookService.query().subscribe((res: HttpResponse<IImportBook[]>) => (this.importbooks = res.body || []));
    });
  }

  updateForm(importDetails: IImportDetails): void {
    this.editForm.patchValue({
      id: importDetails.id,
      amount: importDetails.amount,
      importPrice: importDetails.importPrice,
      bookId: importDetails.bookId,
      importBookId: importDetails.importBookId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const importDetails = this.createFromForm();
    if (importDetails.id !== undefined) {
      this.subscribeToSaveResponse(this.importDetailsService.update(importDetails));
    } else {
      this.subscribeToSaveResponse(this.importDetailsService.create(importDetails));
    }
  }

  private createFromForm(): IImportDetails {
    return {
      ...new ImportDetails(),
      id: this.editForm.get(['id'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      importPrice: this.editForm.get(['importPrice'])!.value,
      bookId: this.editForm.get(['bookId'])!.value,
      importBookId: this.editForm.get(['importBookId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IImportDetails>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
