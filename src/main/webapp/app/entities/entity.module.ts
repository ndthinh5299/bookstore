import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'book',
        loadChildren: () => import('./book/book.module').then(m => m.BookModule)
      },
      {
        path: 'employee',
        loadChildren: () => import('./employee/employee.module').then(m => m.EmployeeModule)
      },
      {
        path: 'customer',
        loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule)
      },
      {
        path: 'import-book',
        loadChildren: () => import('./import-book/import-book.module').then(m => m.ImportBookModule)
      },
      {
        path: 'import-details',
        loadChildren: () => import('./import-details/import-details.module').then(m => m.ImportDetailsModule)
      },
      {
        path: 'export-book',
        loadChildren: () => import('./export-book/export-book.module').then(m => m.ExportBookModule)
      },
      {
        path: 'export-details',
        loadChildren: () => import('./export-details/export-details.module').then(m => m.ExportDetailsModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class EntityModule {}
