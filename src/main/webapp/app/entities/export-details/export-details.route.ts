import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IExportDetails, ExportDetails } from 'app/shared/model/export-details.model';
import { ExportDetailsService } from './export-details.service';
import { ExportDetailsComponent } from './export-details.component';
import { ExportDetailsDetailComponent } from './export-details-detail.component';
import { ExportDetailsUpdateComponent } from './export-details-update.component';

@Injectable({ providedIn: 'root' })
export class ExportDetailsResolve implements Resolve<IExportDetails> {
  constructor(private service: ExportDetailsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IExportDetails> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((exportDetails: HttpResponse<ExportDetails>) => {
          if (exportDetails.body) {
            return of(exportDetails.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ExportDetails());
  }
}

export const exportDetailsRoute: Routes = [
  {
    path: '',
    component: ExportDetailsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'App.exportDetails.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ExportDetailsDetailComponent,
    resolve: {
      exportDetails: ExportDetailsResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.exportDetails.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ExportDetailsUpdateComponent,
    resolve: {
      exportDetails: ExportDetailsResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.exportDetails.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ExportDetailsUpdateComponent,
    resolve: {
      exportDetails: ExportDetailsResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.exportDetails.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
