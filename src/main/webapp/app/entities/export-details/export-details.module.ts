import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { ExportDetailsComponent } from './export-details.component';
import { ExportDetailsDetailComponent } from './export-details-detail.component';
import { ExportDetailsUpdateComponent } from './export-details-update.component';
import { ExportDetailsDeleteDialogComponent } from './export-details-delete-dialog.component';
import { exportDetailsRoute } from './export-details.route';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(exportDetailsRoute)],
  declarations: [ExportDetailsComponent, ExportDetailsDetailComponent, ExportDetailsUpdateComponent, ExportDetailsDeleteDialogComponent],
  entryComponents: [ExportDetailsDeleteDialogComponent]
})
export class ExportDetailsModule {}
