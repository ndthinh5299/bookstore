import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IExportDetails, ExportDetails } from 'app/shared/model/export-details.model';
import { ExportDetailsService } from './export-details.service';
import { IBook } from 'app/shared/model/book.model';
import { BookService } from 'app/entities/book/book.service';
import { IExportBook } from 'app/shared/model/export-book.model';
import { ExportBookService } from 'app/entities/export-book/export-book.service';

type SelectableEntity = IBook | IExportBook;

@Component({
  selector: 'jhi-export-details-update',
  templateUrl: './export-details-update.component.html'
})
export class ExportDetailsUpdateComponent implements OnInit {
  isSaving = false;
  books: IBook[] = [];
  exportbooks: IExportBook[] = [];

  editForm = this.fb.group({
    id: [],
    amount: [],
    importPrice: [],
    bookId: [],
    exportBookId: []
  });

  constructor(
    protected exportDetailsService: ExportDetailsService,
    protected bookService: BookService,
    protected exportBookService: ExportBookService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ exportDetails }) => {
      this.updateForm(exportDetails);

      this.bookService
        .query({ 'exportDetailsId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IBook[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IBook[]) => {
          if (!exportDetails.bookId) {
            this.books = resBody;
          } else {
            this.bookService
              .find(exportDetails.bookId)
              .pipe(
                map((subRes: HttpResponse<IBook>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IBook[]) => (this.books = concatRes));
          }
        });

      this.exportBookService.query().subscribe((res: HttpResponse<IExportBook[]>) => (this.exportbooks = res.body || []));
    });
  }

  updateForm(exportDetails: IExportDetails): void {
    this.editForm.patchValue({
      id: exportDetails.id,
      amount: exportDetails.amount,
      importPrice: exportDetails.importPrice,
      bookId: exportDetails.bookId,
      exportBookId: exportDetails.exportBookId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const exportDetails = this.createFromForm();
    if (exportDetails.id !== undefined) {
      this.subscribeToSaveResponse(this.exportDetailsService.update(exportDetails));
    } else {
      this.subscribeToSaveResponse(this.exportDetailsService.create(exportDetails));
    }
  }

  private createFromForm(): IExportDetails {
    return {
      ...new ExportDetails(),
      id: this.editForm.get(['id'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      importPrice: this.editForm.get(['importPrice'])!.value,
      bookId: this.editForm.get(['bookId'])!.value,
      exportBookId: this.editForm.get(['exportBookId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExportDetails>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
