import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { IExportDetails } from 'app/shared/model/export-details.model';

type EntityResponseType = HttpResponse<IExportDetails>;
type EntityArrayResponseType = HttpResponse<IExportDetails[]>;

@Injectable({ providedIn: 'root' })
export class ExportDetailsService {
  public resourceUrl = SERVER_API_URL + 'api/export-details';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/export-details';

  constructor(protected http: HttpClient) {}

  create(exportDetails: IExportDetails): Observable<EntityResponseType> {
    return this.http.post<IExportDetails>(this.resourceUrl, exportDetails, { observe: 'response' });
  }

  update(exportDetails: IExportDetails): Observable<EntityResponseType> {
    return this.http.put<IExportDetails>(this.resourceUrl, exportDetails, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IExportDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExportDetails[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExportDetails[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
