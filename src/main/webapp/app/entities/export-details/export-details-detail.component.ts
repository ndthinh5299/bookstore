import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExportDetails } from 'app/shared/model/export-details.model';

@Component({
  selector: 'jhi-export-details-detail',
  templateUrl: './export-details-detail.component.html'
})
export class ExportDetailsDetailComponent implements OnInit {
  exportDetails: IExportDetails | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ exportDetails }) => (this.exportDetails = exportDetails));
  }

  previousState(): void {
    window.history.back();
  }
}
