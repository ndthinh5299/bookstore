import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IExportDetails } from 'app/shared/model/export-details.model';
import { ExportDetailsService } from './export-details.service';

@Component({
  templateUrl: './export-details-delete-dialog.component.html'
})
export class ExportDetailsDeleteDialogComponent {
  exportDetails?: IExportDetails;

  constructor(
    protected exportDetailsService: ExportDetailsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.exportDetailsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('exportDetailsListModification');
      this.activeModal.close();
    });
  }
}
