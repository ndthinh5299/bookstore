import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IImportBook } from 'app/shared/model/import-book.model';

@Component({
  selector: 'jhi-import-book-detail',
  templateUrl: './import-book-detail.component.html'
})
export class ImportBookDetailComponent implements OnInit {
  importBook: IImportBook | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ importBook }) => (this.importBook = importBook));
  }

  previousState(): void {
    window.history.back();
  }
}
