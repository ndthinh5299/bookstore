import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { IImportBook } from 'app/shared/model/import-book.model';

type EntityResponseType = HttpResponse<IImportBook>;
type EntityArrayResponseType = HttpResponse<IImportBook[]>;

@Injectable({ providedIn: 'root' })
export class ImportBookService {
  public resourceUrl = SERVER_API_URL + 'api/import-books';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/import-books';

  constructor(protected http: HttpClient) {}

  create(importBook: IImportBook): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(importBook);
    return this.http
      .post<IImportBook>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(importBook: IImportBook): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(importBook);
    return this.http
      .put<IImportBook>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IImportBook>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IImportBook[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IImportBook[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(importBook: IImportBook): IImportBook {
    const copy: IImportBook = Object.assign({}, importBook, {
      importDate: importBook.importDate && importBook.importDate.isValid() ? importBook.importDate.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.importDate = res.body.importDate ? moment(res.body.importDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((importBook: IImportBook) => {
        importBook.importDate = importBook.importDate ? moment(importBook.importDate) : undefined;
      });
    }
    return res;
  }
}
