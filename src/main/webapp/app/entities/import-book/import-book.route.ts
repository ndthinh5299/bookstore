import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IImportBook, ImportBook } from 'app/shared/model/import-book.model';
import { ImportBookService } from './import-book.service';
import { ImportBookComponent } from './import-book.component';
import { ImportBookDetailComponent } from './import-book-detail.component';
import { ImportBookUpdateComponent } from './import-book-update.component';

@Injectable({ providedIn: 'root' })
export class ImportBookResolve implements Resolve<IImportBook> {
  constructor(private service: ImportBookService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IImportBook> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((importBook: HttpResponse<ImportBook>) => {
          if (importBook.body) {
            return of(importBook.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ImportBook());
  }
}

export const importBookRoute: Routes = [
  {
    path: '',
    component: ImportBookComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'App.importBook.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ImportBookDetailComponent,
    resolve: {
      importBook: ImportBookResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.importBook.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ImportBookUpdateComponent,
    resolve: {
      importBook: ImportBookResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.importBook.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ImportBookUpdateComponent,
    resolve: {
      importBook: ImportBookResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'App.importBook.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
