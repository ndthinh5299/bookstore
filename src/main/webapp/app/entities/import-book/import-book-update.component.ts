import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IImportBook, ImportBook } from 'app/shared/model/import-book.model';
import { ImportBookService } from './import-book.service';
import { IEmployee } from 'app/shared/model/employee.model';
import { EmployeeService } from 'app/entities/employee/employee.service';

@Component({
  selector: 'jhi-import-book-update',
  templateUrl: './import-book-update.component.html'
})
export class ImportBookUpdateComponent implements OnInit {
  isSaving = false;
  employees: IEmployee[] = [];

  editForm = this.fb.group({
    id: [],
    importDate: [],
    provider: [],
    employeeId: []
  });

  constructor(
    protected importBookService: ImportBookService,
    protected employeeService: EmployeeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ importBook }) => {
      if (!importBook.id) {
        const today = moment().startOf('day');
        importBook.importDate = today;
      }

      this.updateForm(importBook);

      this.employeeService
        .query({ 'importBookId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IEmployee[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IEmployee[]) => {
          if (!importBook.employeeId) {
            this.employees = resBody;
          } else {
            this.employeeService
              .find(importBook.employeeId)
              .pipe(
                map((subRes: HttpResponse<IEmployee>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IEmployee[]) => (this.employees = concatRes));
          }
        });
    });
  }

  updateForm(importBook: IImportBook): void {
    this.editForm.patchValue({
      id: importBook.id,
      importDate: importBook.importDate ? importBook.importDate.format(DATE_TIME_FORMAT) : null,
      provider: importBook.provider,
      employeeId: importBook.employeeId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const importBook = this.createFromForm();
    if (importBook.id !== undefined) {
      this.subscribeToSaveResponse(this.importBookService.update(importBook));
    } else {
      this.subscribeToSaveResponse(this.importBookService.create(importBook));
    }
  }

  private createFromForm(): IImportBook {
    return {
      ...new ImportBook(),
      id: this.editForm.get(['id'])!.value,
      importDate: this.editForm.get(['importDate'])!.value ? moment(this.editForm.get(['importDate'])!.value, DATE_TIME_FORMAT) : undefined,
      provider: this.editForm.get(['provider'])!.value,
      employeeId: this.editForm.get(['employeeId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IImportBook>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IEmployee): any {
    return item.id;
  }
}
