import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { ImportBookComponent } from './import-book.component';
import { ImportBookDetailComponent } from './import-book-detail.component';
import { ImportBookUpdateComponent } from './import-book-update.component';
import { ImportBookDeleteDialogComponent } from './import-book-delete-dialog.component';
import { importBookRoute } from './import-book.route';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(importBookRoute)],
  declarations: [ImportBookComponent, ImportBookDetailComponent, ImportBookUpdateComponent, ImportBookDeleteDialogComponent],
  entryComponents: [ImportBookDeleteDialogComponent]
})
export class ImportBookModule {}
