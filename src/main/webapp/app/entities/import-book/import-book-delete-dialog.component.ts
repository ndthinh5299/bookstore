import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IImportBook } from 'app/shared/model/import-book.model';
import { ImportBookService } from './import-book.service';

@Component({
  templateUrl: './import-book-delete-dialog.component.html'
})
export class ImportBookDeleteDialogComponent {
  importBook?: IImportBook;

  constructor(
    protected importBookService: ImportBookService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.importBookService.delete(id).subscribe(() => {
      this.eventManager.broadcast('importBookListModification');
      this.activeModal.close();
    });
  }
}
