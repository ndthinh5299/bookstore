import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TestModule } from '../../../test.module';
import { ImportBookDetailComponent } from 'app/entities/import-book/import-book-detail.component';
import { ImportBook } from 'app/shared/model/import-book.model';

describe('Component Tests', () => {
  describe('ImportBook Management Detail Component', () => {
    let comp: ImportBookDetailComponent;
    let fixture: ComponentFixture<ImportBookDetailComponent>;
    const route = ({ data: of({ importBook: new ImportBook(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TestModule],
        declarations: [ImportBookDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ImportBookDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ImportBookDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load importBook on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.importBook).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
