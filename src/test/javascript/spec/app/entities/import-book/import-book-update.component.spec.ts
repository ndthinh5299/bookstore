import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TestModule } from '../../../test.module';
import { ImportBookUpdateComponent } from 'app/entities/import-book/import-book-update.component';
import { ImportBookService } from 'app/entities/import-book/import-book.service';
import { ImportBook } from 'app/shared/model/import-book.model';

describe('Component Tests', () => {
  describe('ImportBook Management Update Component', () => {
    let comp: ImportBookUpdateComponent;
    let fixture: ComponentFixture<ImportBookUpdateComponent>;
    let service: ImportBookService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TestModule],
        declarations: [ImportBookUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ImportBookUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ImportBookUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ImportBookService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ImportBook(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ImportBook();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
