import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TestModule } from '../../../test.module';
import { ExportBookUpdateComponent } from 'app/entities/export-book/export-book-update.component';
import { ExportBookService } from 'app/entities/export-book/export-book.service';
import { ExportBook } from 'app/shared/model/export-book.model';

describe('Component Tests', () => {
  describe('ExportBook Management Update Component', () => {
    let comp: ExportBookUpdateComponent;
    let fixture: ComponentFixture<ExportBookUpdateComponent>;
    let service: ExportBookService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TestModule],
        declarations: [ExportBookUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ExportBookUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExportBookUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExportBookService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExportBook(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExportBook();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
