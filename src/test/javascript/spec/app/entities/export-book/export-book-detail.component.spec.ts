import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TestModule } from '../../../test.module';
import { ExportBookDetailComponent } from 'app/entities/export-book/export-book-detail.component';
import { ExportBook } from 'app/shared/model/export-book.model';

describe('Component Tests', () => {
  describe('ExportBook Management Detail Component', () => {
    let comp: ExportBookDetailComponent;
    let fixture: ComponentFixture<ExportBookDetailComponent>;
    const route = ({ data: of({ exportBook: new ExportBook(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TestModule],
        declarations: [ExportBookDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ExportBookDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExportBookDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load exportBook on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.exportBook).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
