import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { ExportBookService } from 'app/entities/export-book/export-book.service';
import { IExportBook, ExportBook } from 'app/shared/model/export-book.model';

describe('Service Tests', () => {
  describe('ExportBook Service', () => {
    let injector: TestBed;
    let service: ExportBookService;
    let httpMock: HttpTestingController;
    let elemDefault: IExportBook;
    let expectedResult: IExportBook | IExportBook[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ExportBookService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new ExportBook(0, currentDate, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            exportDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ExportBook', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            exportDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            exportDate: currentDate
          },
          returnedFromService
        );

        service.create(new ExportBook()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ExportBook', () => {
        const returnedFromService = Object.assign(
          {
            exportDate: currentDate.format(DATE_TIME_FORMAT),
            totalAmount: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            exportDate: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ExportBook', () => {
        const returnedFromService = Object.assign(
          {
            exportDate: currentDate.format(DATE_TIME_FORMAT),
            totalAmount: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            exportDate: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ExportBook', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
