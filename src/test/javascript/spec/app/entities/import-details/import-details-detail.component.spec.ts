import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TestModule } from '../../../test.module';
import { ImportDetailsDetailComponent } from 'app/entities/import-details/import-details-detail.component';
import { ImportDetails } from 'app/shared/model/import-details.model';

describe('Component Tests', () => {
  describe('ImportDetails Management Detail Component', () => {
    let comp: ImportDetailsDetailComponent;
    let fixture: ComponentFixture<ImportDetailsDetailComponent>;
    const route = ({ data: of({ importDetails: new ImportDetails(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TestModule],
        declarations: [ImportDetailsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ImportDetailsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ImportDetailsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load importDetails on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.importDetails).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
