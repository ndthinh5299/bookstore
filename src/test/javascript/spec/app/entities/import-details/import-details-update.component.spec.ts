import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TestModule } from '../../../test.module';
import { ImportDetailsUpdateComponent } from 'app/entities/import-details/import-details-update.component';
import { ImportDetailsService } from 'app/entities/import-details/import-details.service';
import { ImportDetails } from 'app/shared/model/import-details.model';

describe('Component Tests', () => {
  describe('ImportDetails Management Update Component', () => {
    let comp: ImportDetailsUpdateComponent;
    let fixture: ComponentFixture<ImportDetailsUpdateComponent>;
    let service: ImportDetailsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TestModule],
        declarations: [ImportDetailsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ImportDetailsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ImportDetailsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ImportDetailsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ImportDetails(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ImportDetails();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
