import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TestModule } from '../../../test.module';
import { ExportDetailsDetailComponent } from 'app/entities/export-details/export-details-detail.component';
import { ExportDetails } from 'app/shared/model/export-details.model';

describe('Component Tests', () => {
  describe('ExportDetails Management Detail Component', () => {
    let comp: ExportDetailsDetailComponent;
    let fixture: ComponentFixture<ExportDetailsDetailComponent>;
    const route = ({ data: of({ exportDetails: new ExportDetails(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TestModule],
        declarations: [ExportDetailsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ExportDetailsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExportDetailsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load exportDetails on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.exportDetails).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
