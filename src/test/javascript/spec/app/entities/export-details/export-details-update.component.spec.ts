import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TestModule } from '../../../test.module';
import { ExportDetailsUpdateComponent } from 'app/entities/export-details/export-details-update.component';
import { ExportDetailsService } from 'app/entities/export-details/export-details.service';
import { ExportDetails } from 'app/shared/model/export-details.model';

describe('Component Tests', () => {
  describe('ExportDetails Management Update Component', () => {
    let comp: ExportDetailsUpdateComponent;
    let fixture: ComponentFixture<ExportDetailsUpdateComponent>;
    let service: ExportDetailsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TestModule],
        declarations: [ExportDetailsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ExportDetailsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExportDetailsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExportDetailsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExportDetails(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExportDetails();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
