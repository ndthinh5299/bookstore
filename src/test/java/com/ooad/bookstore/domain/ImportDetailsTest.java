package com.ooad.bookstore.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ooad.bookstore.web.rest.TestUtil;

public class ImportDetailsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImportDetails.class);
        ImportDetails importDetails1 = new ImportDetails();
        importDetails1.setId(1L);
        ImportDetails importDetails2 = new ImportDetails();
        importDetails2.setId(importDetails1.getId());
        assertThat(importDetails1).isEqualTo(importDetails2);
        importDetails2.setId(2L);
        assertThat(importDetails1).isNotEqualTo(importDetails2);
        importDetails1.setId(null);
        assertThat(importDetails1).isNotEqualTo(importDetails2);
    }
}
