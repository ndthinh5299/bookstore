package com.ooad.bookstore.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ooad.bookstore.web.rest.TestUtil;

public class ExportDetailsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExportDetails.class);
        ExportDetails exportDetails1 = new ExportDetails();
        exportDetails1.setId(1L);
        ExportDetails exportDetails2 = new ExportDetails();
        exportDetails2.setId(exportDetails1.getId());
        assertThat(exportDetails1).isEqualTo(exportDetails2);
        exportDetails2.setId(2L);
        assertThat(exportDetails1).isNotEqualTo(exportDetails2);
        exportDetails1.setId(null);
        assertThat(exportDetails1).isNotEqualTo(exportDetails2);
    }
}
