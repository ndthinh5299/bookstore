package com.ooad.bookstore.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ooad.bookstore.web.rest.TestUtil;

public class ImportBookTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImportBook.class);
        ImportBook importBook1 = new ImportBook();
        importBook1.setId(1L);
        ImportBook importBook2 = new ImportBook();
        importBook2.setId(importBook1.getId());
        assertThat(importBook1).isEqualTo(importBook2);
        importBook2.setId(2L);
        assertThat(importBook1).isNotEqualTo(importBook2);
        importBook1.setId(null);
        assertThat(importBook1).isNotEqualTo(importBook2);
    }
}
