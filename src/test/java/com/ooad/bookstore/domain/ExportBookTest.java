package com.ooad.bookstore.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ooad.bookstore.web.rest.TestUtil;

public class ExportBookTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExportBook.class);
        ExportBook exportBook1 = new ExportBook();
        exportBook1.setId(1L);
        ExportBook exportBook2 = new ExportBook();
        exportBook2.setId(exportBook1.getId());
        assertThat(exportBook1).isEqualTo(exportBook2);
        exportBook2.setId(2L);
        assertThat(exportBook1).isNotEqualTo(exportBook2);
        exportBook1.setId(null);
        assertThat(exportBook1).isNotEqualTo(exportBook2);
    }
}
