package com.ooad.bookstore.web.rest;


import com.ooad.bookstore.domain.ImportBook;
import com.ooad.bookstore.domain.Employee;
import com.ooad.bookstore.domain.ImportDetails;
import com.ooad.bookstore.repository.ImportBookRepository;
import com.ooad.bookstore.repository.search.ImportBookSearchRepository;
import com.ooad.bookstore.service.ImportBookService;
import com.ooad.bookstore.service.dto.ImportBookDTO;
import com.ooad.bookstore.service.mapper.ImportBookMapper;
import com.ooad.bookstore.service.dto.ImportBookCriteria;
import com.ooad.bookstore.service.ImportBookQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ImportBookResource} REST controller.
 */

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ImportBookResourceIT {

    private static final Instant DEFAULT_IMPORT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_IMPORT_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_PROVIDER = "AAAAAAAAAA";
    private static final String UPDATED_PROVIDER = "BBBBBBBBBB";

    @Autowired
    private ImportBookRepository importBookRepository;

    @Autowired
    private ImportBookMapper importBookMapper;

    @Autowired
    private ImportBookService importBookService;

    /**
     * This repository is mocked in the com.ooad.bookstore.repository.search test package.
     *
     * @see com.ooad.bookstore.repository.search.ImportBookSearchRepositoryMockConfiguration
     */
    @Autowired
    private ImportBookSearchRepository mockImportBookSearchRepository;

    @Autowired
    private ImportBookQueryService importBookQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restImportBookMockMvc;

    private ImportBook importBook;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImportBook createEntity(EntityManager em) {
        ImportBook importBook = new ImportBook()
            .importDate(DEFAULT_IMPORT_DATE)
            .provider(DEFAULT_PROVIDER);
        return importBook;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImportBook createUpdatedEntity(EntityManager em) {
        ImportBook importBook = new ImportBook()
            .importDate(UPDATED_IMPORT_DATE)
            .provider(UPDATED_PROVIDER);
        return importBook;
    }

    @BeforeEach
    public void initTest() {
        importBook = createEntity(em);
    }

    @Test
    @Transactional
    public void createImportBook() throws Exception {
        int databaseSizeBeforeCreate = importBookRepository.findAll().size();

        // Create the ImportBook
        ImportBookDTO importBookDTO = importBookMapper.toDto(importBook);
        restImportBookMockMvc.perform(post("/api/import-books")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(importBookDTO)))
            .andExpect(status().isCreated());

        // Validate the ImportBook in the database
        List<ImportBook> importBookList = importBookRepository.findAll();
        assertThat(importBookList).hasSize(databaseSizeBeforeCreate + 1);
        ImportBook testImportBook = importBookList.get(importBookList.size() - 1);
        assertThat(testImportBook.getImportDate()).isEqualTo(DEFAULT_IMPORT_DATE);
        assertThat(testImportBook.getProvider()).isEqualTo(DEFAULT_PROVIDER);

        // Validate the ImportBook in Elasticsearch
        verify(mockImportBookSearchRepository, times(1)).save(testImportBook);
    }

    @Test
    @Transactional
    public void createImportBookWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = importBookRepository.findAll().size();

        // Create the ImportBook with an existing ID
        importBook.setId(1L);
        ImportBookDTO importBookDTO = importBookMapper.toDto(importBook);

        // An entity with an existing ID cannot be created, so this API call must fail
        restImportBookMockMvc.perform(post("/api/import-books")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(importBookDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ImportBook in the database
        List<ImportBook> importBookList = importBookRepository.findAll();
        assertThat(importBookList).hasSize(databaseSizeBeforeCreate);

        // Validate the ImportBook in Elasticsearch
        verify(mockImportBookSearchRepository, times(0)).save(importBook);
    }


    @Test
    @Transactional
    public void getAllImportBooks() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList
        restImportBookMockMvc.perform(get("/api/import-books?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(importBook.getId().intValue())))
            .andExpect(jsonPath("$.[*].importDate").value(hasItem(DEFAULT_IMPORT_DATE.toString())))
            .andExpect(jsonPath("$.[*].provider").value(hasItem(DEFAULT_PROVIDER)));
    }

    @Test
    @Transactional
    public void getImportBook() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get the importBook
        restImportBookMockMvc.perform(get("/api/import-books/{id}", importBook.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(importBook.getId().intValue()))
            .andExpect(jsonPath("$.importDate").value(DEFAULT_IMPORT_DATE.toString()))
            .andExpect(jsonPath("$.provider").value(DEFAULT_PROVIDER));
    }


    @Test
    @Transactional
    public void getImportBooksByIdFiltering() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        Long id = importBook.getId();

        defaultImportBookShouldBeFound("id.equals=" + id);
        defaultImportBookShouldNotBeFound("id.notEquals=" + id);

        defaultImportBookShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultImportBookShouldNotBeFound("id.greaterThan=" + id);

        defaultImportBookShouldBeFound("id.lessThanOrEqual=" + id);
        defaultImportBookShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllImportBooksByImportDateIsEqualToSomething() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList where importDate equals to DEFAULT_IMPORT_DATE
        defaultImportBookShouldBeFound("importDate.equals=" + DEFAULT_IMPORT_DATE);

        // Get all the importBookList where importDate equals to UPDATED_IMPORT_DATE
        defaultImportBookShouldNotBeFound("importDate.equals=" + UPDATED_IMPORT_DATE);
    }

    @Test
    @Transactional
    public void getAllImportBooksByImportDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList where importDate not equals to DEFAULT_IMPORT_DATE
        defaultImportBookShouldNotBeFound("importDate.notEquals=" + DEFAULT_IMPORT_DATE);

        // Get all the importBookList where importDate not equals to UPDATED_IMPORT_DATE
        defaultImportBookShouldBeFound("importDate.notEquals=" + UPDATED_IMPORT_DATE);
    }

    @Test
    @Transactional
    public void getAllImportBooksByImportDateIsInShouldWork() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList where importDate in DEFAULT_IMPORT_DATE or UPDATED_IMPORT_DATE
        defaultImportBookShouldBeFound("importDate.in=" + DEFAULT_IMPORT_DATE + "," + UPDATED_IMPORT_DATE);

        // Get all the importBookList where importDate equals to UPDATED_IMPORT_DATE
        defaultImportBookShouldNotBeFound("importDate.in=" + UPDATED_IMPORT_DATE);
    }

    @Test
    @Transactional
    public void getAllImportBooksByImportDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList where importDate is not null
        defaultImportBookShouldBeFound("importDate.specified=true");

        // Get all the importBookList where importDate is null
        defaultImportBookShouldNotBeFound("importDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllImportBooksByProviderIsEqualToSomething() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList where provider equals to DEFAULT_PROVIDER
        defaultImportBookShouldBeFound("provider.equals=" + DEFAULT_PROVIDER);

        // Get all the importBookList where provider equals to UPDATED_PROVIDER
        defaultImportBookShouldNotBeFound("provider.equals=" + UPDATED_PROVIDER);
    }

    @Test
    @Transactional
    public void getAllImportBooksByProviderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList where provider not equals to DEFAULT_PROVIDER
        defaultImportBookShouldNotBeFound("provider.notEquals=" + DEFAULT_PROVIDER);

        // Get all the importBookList where provider not equals to UPDATED_PROVIDER
        defaultImportBookShouldBeFound("provider.notEquals=" + UPDATED_PROVIDER);
    }

    @Test
    @Transactional
    public void getAllImportBooksByProviderIsInShouldWork() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList where provider in DEFAULT_PROVIDER or UPDATED_PROVIDER
        defaultImportBookShouldBeFound("provider.in=" + DEFAULT_PROVIDER + "," + UPDATED_PROVIDER);

        // Get all the importBookList where provider equals to UPDATED_PROVIDER
        defaultImportBookShouldNotBeFound("provider.in=" + UPDATED_PROVIDER);
    }

    @Test
    @Transactional
    public void getAllImportBooksByProviderIsNullOrNotNull() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList where provider is not null
        defaultImportBookShouldBeFound("provider.specified=true");

        // Get all the importBookList where provider is null
        defaultImportBookShouldNotBeFound("provider.specified=false");
    }
                @Test
    @Transactional
    public void getAllImportBooksByProviderContainsSomething() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList where provider contains DEFAULT_PROVIDER
        defaultImportBookShouldBeFound("provider.contains=" + DEFAULT_PROVIDER);

        // Get all the importBookList where provider contains UPDATED_PROVIDER
        defaultImportBookShouldNotBeFound("provider.contains=" + UPDATED_PROVIDER);
    }

    @Test
    @Transactional
    public void getAllImportBooksByProviderNotContainsSomething() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        // Get all the importBookList where provider does not contain DEFAULT_PROVIDER
        defaultImportBookShouldNotBeFound("provider.doesNotContain=" + DEFAULT_PROVIDER);

        // Get all the importBookList where provider does not contain UPDATED_PROVIDER
        defaultImportBookShouldBeFound("provider.doesNotContain=" + UPDATED_PROVIDER);
    }


    @Test
    @Transactional
    public void getAllImportBooksByEmployeeIsEqualToSomething() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);
        Employee employee = EmployeeResourceIT.createEntity(em);
        em.persist(employee);
        em.flush();
        importBook.setEmployee(employee);
        importBookRepository.saveAndFlush(importBook);
        Long employeeId = employee.getId();

        // Get all the importBookList where employee equals to employeeId
        defaultImportBookShouldBeFound("employeeId.equals=" + employeeId);

        // Get all the importBookList where employee equals to employeeId + 1
        defaultImportBookShouldNotBeFound("employeeId.equals=" + (employeeId + 1));
    }


    @Test
    @Transactional
    public void getAllImportBooksByImportDetailsIsEqualToSomething() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);
        ImportDetails importDetails = ImportDetailsResourceIT.createEntity(em);
        em.persist(importDetails);
        em.flush();
        importBook.addImportDetails(importDetails);
        importBookRepository.saveAndFlush(importBook);
        Long importDetailsId = importDetails.getId();

        // Get all the importBookList where importDetails equals to importDetailsId
        defaultImportBookShouldBeFound("importDetailsId.equals=" + importDetailsId);

        // Get all the importBookList where importDetails equals to importDetailsId + 1
        defaultImportBookShouldNotBeFound("importDetailsId.equals=" + (importDetailsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultImportBookShouldBeFound(String filter) throws Exception {
        restImportBookMockMvc.perform(get("/api/import-books?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(importBook.getId().intValue())))
            .andExpect(jsonPath("$.[*].importDate").value(hasItem(DEFAULT_IMPORT_DATE.toString())))
            .andExpect(jsonPath("$.[*].provider").value(hasItem(DEFAULT_PROVIDER)));

        // Check, that the count call also returns 1
        restImportBookMockMvc.perform(get("/api/import-books/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultImportBookShouldNotBeFound(String filter) throws Exception {
        restImportBookMockMvc.perform(get("/api/import-books?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restImportBookMockMvc.perform(get("/api/import-books/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingImportBook() throws Exception {
        // Get the importBook
        restImportBookMockMvc.perform(get("/api/import-books/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImportBook() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        int databaseSizeBeforeUpdate = importBookRepository.findAll().size();

        // Update the importBook
        ImportBook updatedImportBook = importBookRepository.findById(importBook.getId()).get();
        // Disconnect from session so that the updates on updatedImportBook are not directly saved in db
        em.detach(updatedImportBook);
        updatedImportBook
            .importDate(UPDATED_IMPORT_DATE)
            .provider(UPDATED_PROVIDER);
        ImportBookDTO importBookDTO = importBookMapper.toDto(updatedImportBook);

        restImportBookMockMvc.perform(put("/api/import-books")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(importBookDTO)))
            .andExpect(status().isOk());

        // Validate the ImportBook in the database
        List<ImportBook> importBookList = importBookRepository.findAll();
        assertThat(importBookList).hasSize(databaseSizeBeforeUpdate);
        ImportBook testImportBook = importBookList.get(importBookList.size() - 1);
        assertThat(testImportBook.getImportDate()).isEqualTo(UPDATED_IMPORT_DATE);
        assertThat(testImportBook.getProvider()).isEqualTo(UPDATED_PROVIDER);

        // Validate the ImportBook in Elasticsearch
        verify(mockImportBookSearchRepository, times(1)).save(testImportBook);
    }

    @Test
    @Transactional
    public void updateNonExistingImportBook() throws Exception {
        int databaseSizeBeforeUpdate = importBookRepository.findAll().size();

        // Create the ImportBook
        ImportBookDTO importBookDTO = importBookMapper.toDto(importBook);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImportBookMockMvc.perform(put("/api/import-books")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(importBookDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ImportBook in the database
        List<ImportBook> importBookList = importBookRepository.findAll();
        assertThat(importBookList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ImportBook in Elasticsearch
        verify(mockImportBookSearchRepository, times(0)).save(importBook);
    }

    @Test
    @Transactional
    public void deleteImportBook() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);

        int databaseSizeBeforeDelete = importBookRepository.findAll().size();

        // Delete the importBook
        restImportBookMockMvc.perform(delete("/api/import-books/{id}", importBook.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ImportBook> importBookList = importBookRepository.findAll();
        assertThat(importBookList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ImportBook in Elasticsearch
        verify(mockImportBookSearchRepository, times(1)).deleteById(importBook.getId());
    }

    @Test
    @Transactional
    public void searchImportBook() throws Exception {
        // Initialize the database
        importBookRepository.saveAndFlush(importBook);
        when(mockImportBookSearchRepository.search(queryStringQuery("id:" + importBook.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(importBook), PageRequest.of(0, 1), 1));
        // Search the importBook
        restImportBookMockMvc.perform(get("/api/_search/import-books?query=id:" + importBook.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(importBook.getId().intValue())))
            .andExpect(jsonPath("$.[*].importDate").value(hasItem(DEFAULT_IMPORT_DATE.toString())))
            .andExpect(jsonPath("$.[*].provider").value(hasItem(DEFAULT_PROVIDER)));
    }
}
