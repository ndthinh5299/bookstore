package com.ooad.bookstore.web.rest;


import com.ooad.bookstore.domain.ImportDetails;
import com.ooad.bookstore.domain.Book;
import com.ooad.bookstore.domain.ImportBook;
import com.ooad.bookstore.repository.ImportDetailsRepository;
import com.ooad.bookstore.repository.search.ImportDetailsSearchRepository;
import com.ooad.bookstore.service.ImportDetailsService;
import com.ooad.bookstore.service.dto.ImportDetailsDTO;
import com.ooad.bookstore.service.mapper.ImportDetailsMapper;
import com.ooad.bookstore.service.dto.ImportDetailsCriteria;
import com.ooad.bookstore.service.ImportDetailsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ImportDetailsResource} REST controller.
 */

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ImportDetailsResourceIT {

    private static final Integer DEFAULT_AMOUNT = 1;
    private static final Integer UPDATED_AMOUNT = 2;
    private static final Integer SMALLER_AMOUNT = 1 - 1;

    private static final Float DEFAULT_IMPORT_PRICE = 1F;
    private static final Float UPDATED_IMPORT_PRICE = 2F;
    private static final Float SMALLER_IMPORT_PRICE = 1F - 1F;

    @Autowired
    private ImportDetailsRepository importDetailsRepository;

    @Autowired
    private ImportDetailsMapper importDetailsMapper;

    @Autowired
    private ImportDetailsService importDetailsService;

    /**
     * This repository is mocked in the com.ooad.bookstore.repository.search test package.
     *
     * @see com.ooad.bookstore.repository.search.ImportDetailsSearchRepositoryMockConfiguration
     */
    @Autowired
    private ImportDetailsSearchRepository mockImportDetailsSearchRepository;

    @Autowired
    private ImportDetailsQueryService importDetailsQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restImportDetailsMockMvc;

    private ImportDetails importDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImportDetails createEntity(EntityManager em) {
        ImportDetails importDetails = new ImportDetails()
            .amount(DEFAULT_AMOUNT)
            .importPrice(DEFAULT_IMPORT_PRICE);
        return importDetails;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImportDetails createUpdatedEntity(EntityManager em) {
        ImportDetails importDetails = new ImportDetails()
            .amount(UPDATED_AMOUNT)
            .importPrice(UPDATED_IMPORT_PRICE);
        return importDetails;
    }

    @BeforeEach
    public void initTest() {
        importDetails = createEntity(em);
    }

    @Test
    @Transactional
    public void createImportDetails() throws Exception {
        int databaseSizeBeforeCreate = importDetailsRepository.findAll().size();

        // Create the ImportDetails
        ImportDetailsDTO importDetailsDTO = importDetailsMapper.toDto(importDetails);
        restImportDetailsMockMvc.perform(post("/api/import-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(importDetailsDTO)))
            .andExpect(status().isCreated());

        // Validate the ImportDetails in the database
        List<ImportDetails> importDetailsList = importDetailsRepository.findAll();
        assertThat(importDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        ImportDetails testImportDetails = importDetailsList.get(importDetailsList.size() - 1);
        assertThat(testImportDetails.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testImportDetails.getImportPrice()).isEqualTo(DEFAULT_IMPORT_PRICE);

        // Validate the ImportDetails in Elasticsearch
        verify(mockImportDetailsSearchRepository, times(1)).save(testImportDetails);
    }

    @Test
    @Transactional
    public void createImportDetailsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = importDetailsRepository.findAll().size();

        // Create the ImportDetails with an existing ID
        importDetails.setId(1L);
        ImportDetailsDTO importDetailsDTO = importDetailsMapper.toDto(importDetails);

        // An entity with an existing ID cannot be created, so this API call must fail
        restImportDetailsMockMvc.perform(post("/api/import-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(importDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ImportDetails in the database
        List<ImportDetails> importDetailsList = importDetailsRepository.findAll();
        assertThat(importDetailsList).hasSize(databaseSizeBeforeCreate);

        // Validate the ImportDetails in Elasticsearch
        verify(mockImportDetailsSearchRepository, times(0)).save(importDetails);
    }


    @Test
    @Transactional
    public void getAllImportDetails() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList
        restImportDetailsMockMvc.perform(get("/api/import-details?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(importDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.[*].importPrice").value(hasItem(DEFAULT_IMPORT_PRICE.doubleValue())));
    }

    @Test
    @Transactional
    public void getImportDetails() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get the importDetails
        restImportDetailsMockMvc.perform(get("/api/import-details/{id}", importDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(importDetails.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT))
            .andExpect(jsonPath("$.importPrice").value(DEFAULT_IMPORT_PRICE.doubleValue()));
    }


    @Test
    @Transactional
    public void getImportDetailsByIdFiltering() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        Long id = importDetails.getId();

        defaultImportDetailsShouldBeFound("id.equals=" + id);
        defaultImportDetailsShouldNotBeFound("id.notEquals=" + id);

        defaultImportDetailsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultImportDetailsShouldNotBeFound("id.greaterThan=" + id);

        defaultImportDetailsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultImportDetailsShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllImportDetailsByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where amount equals to DEFAULT_AMOUNT
        defaultImportDetailsShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the importDetailsList where amount equals to UPDATED_AMOUNT
        defaultImportDetailsShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where amount not equals to DEFAULT_AMOUNT
        defaultImportDetailsShouldNotBeFound("amount.notEquals=" + DEFAULT_AMOUNT);

        // Get all the importDetailsList where amount not equals to UPDATED_AMOUNT
        defaultImportDetailsShouldBeFound("amount.notEquals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultImportDetailsShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the importDetailsList where amount equals to UPDATED_AMOUNT
        defaultImportDetailsShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where amount is not null
        defaultImportDetailsShouldBeFound("amount.specified=true");

        // Get all the importDetailsList where amount is null
        defaultImportDetailsShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    public void getAllImportDetailsByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where amount is greater than or equal to DEFAULT_AMOUNT
        defaultImportDetailsShouldBeFound("amount.greaterThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the importDetailsList where amount is greater than or equal to UPDATED_AMOUNT
        defaultImportDetailsShouldNotBeFound("amount.greaterThanOrEqual=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where amount is less than or equal to DEFAULT_AMOUNT
        defaultImportDetailsShouldBeFound("amount.lessThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the importDetailsList where amount is less than or equal to SMALLER_AMOUNT
        defaultImportDetailsShouldNotBeFound("amount.lessThanOrEqual=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where amount is less than DEFAULT_AMOUNT
        defaultImportDetailsShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUNT);

        // Get all the importDetailsList where amount is less than UPDATED_AMOUNT
        defaultImportDetailsShouldBeFound("amount.lessThan=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where amount is greater than DEFAULT_AMOUNT
        defaultImportDetailsShouldNotBeFound("amount.greaterThan=" + DEFAULT_AMOUNT);

        // Get all the importDetailsList where amount is greater than SMALLER_AMOUNT
        defaultImportDetailsShouldBeFound("amount.greaterThan=" + SMALLER_AMOUNT);
    }


    @Test
    @Transactional
    public void getAllImportDetailsByImportPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where importPrice equals to DEFAULT_IMPORT_PRICE
        defaultImportDetailsShouldBeFound("importPrice.equals=" + DEFAULT_IMPORT_PRICE);

        // Get all the importDetailsList where importPrice equals to UPDATED_IMPORT_PRICE
        defaultImportDetailsShouldNotBeFound("importPrice.equals=" + UPDATED_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByImportPriceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where importPrice not equals to DEFAULT_IMPORT_PRICE
        defaultImportDetailsShouldNotBeFound("importPrice.notEquals=" + DEFAULT_IMPORT_PRICE);

        // Get all the importDetailsList where importPrice not equals to UPDATED_IMPORT_PRICE
        defaultImportDetailsShouldBeFound("importPrice.notEquals=" + UPDATED_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByImportPriceIsInShouldWork() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where importPrice in DEFAULT_IMPORT_PRICE or UPDATED_IMPORT_PRICE
        defaultImportDetailsShouldBeFound("importPrice.in=" + DEFAULT_IMPORT_PRICE + "," + UPDATED_IMPORT_PRICE);

        // Get all the importDetailsList where importPrice equals to UPDATED_IMPORT_PRICE
        defaultImportDetailsShouldNotBeFound("importPrice.in=" + UPDATED_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByImportPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where importPrice is not null
        defaultImportDetailsShouldBeFound("importPrice.specified=true");

        // Get all the importDetailsList where importPrice is null
        defaultImportDetailsShouldNotBeFound("importPrice.specified=false");
    }

    @Test
    @Transactional
    public void getAllImportDetailsByImportPriceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where importPrice is greater than or equal to DEFAULT_IMPORT_PRICE
        defaultImportDetailsShouldBeFound("importPrice.greaterThanOrEqual=" + DEFAULT_IMPORT_PRICE);

        // Get all the importDetailsList where importPrice is greater than or equal to UPDATED_IMPORT_PRICE
        defaultImportDetailsShouldNotBeFound("importPrice.greaterThanOrEqual=" + UPDATED_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByImportPriceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where importPrice is less than or equal to DEFAULT_IMPORT_PRICE
        defaultImportDetailsShouldBeFound("importPrice.lessThanOrEqual=" + DEFAULT_IMPORT_PRICE);

        // Get all the importDetailsList where importPrice is less than or equal to SMALLER_IMPORT_PRICE
        defaultImportDetailsShouldNotBeFound("importPrice.lessThanOrEqual=" + SMALLER_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByImportPriceIsLessThanSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where importPrice is less than DEFAULT_IMPORT_PRICE
        defaultImportDetailsShouldNotBeFound("importPrice.lessThan=" + DEFAULT_IMPORT_PRICE);

        // Get all the importDetailsList where importPrice is less than UPDATED_IMPORT_PRICE
        defaultImportDetailsShouldBeFound("importPrice.lessThan=" + UPDATED_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllImportDetailsByImportPriceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        // Get all the importDetailsList where importPrice is greater than DEFAULT_IMPORT_PRICE
        defaultImportDetailsShouldNotBeFound("importPrice.greaterThan=" + DEFAULT_IMPORT_PRICE);

        // Get all the importDetailsList where importPrice is greater than SMALLER_IMPORT_PRICE
        defaultImportDetailsShouldBeFound("importPrice.greaterThan=" + SMALLER_IMPORT_PRICE);
    }


    @Test
    @Transactional
    public void getAllImportDetailsByBookIsEqualToSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);
        Book book = BookResourceIT.createEntity(em);
        em.persist(book);
        em.flush();
        importDetails.setBook(book);
        importDetailsRepository.saveAndFlush(importDetails);
        Long bookId = book.getId();

        // Get all the importDetailsList where book equals to bookId
        defaultImportDetailsShouldBeFound("bookId.equals=" + bookId);

        // Get all the importDetailsList where book equals to bookId + 1
        defaultImportDetailsShouldNotBeFound("bookId.equals=" + (bookId + 1));
    }


    @Test
    @Transactional
    public void getAllImportDetailsByImportBookIsEqualToSomething() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);
        ImportBook importBook = ImportBookResourceIT.createEntity(em);
        em.persist(importBook);
        em.flush();
        importDetails.setImportBook(importBook);
        importDetailsRepository.saveAndFlush(importDetails);
        Long importBookId = importBook.getId();

        // Get all the importDetailsList where importBook equals to importBookId
        defaultImportDetailsShouldBeFound("importBookId.equals=" + importBookId);

        // Get all the importDetailsList where importBook equals to importBookId + 1
        defaultImportDetailsShouldNotBeFound("importBookId.equals=" + (importBookId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultImportDetailsShouldBeFound(String filter) throws Exception {
        restImportDetailsMockMvc.perform(get("/api/import-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(importDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.[*].importPrice").value(hasItem(DEFAULT_IMPORT_PRICE.doubleValue())));

        // Check, that the count call also returns 1
        restImportDetailsMockMvc.perform(get("/api/import-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultImportDetailsShouldNotBeFound(String filter) throws Exception {
        restImportDetailsMockMvc.perform(get("/api/import-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restImportDetailsMockMvc.perform(get("/api/import-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingImportDetails() throws Exception {
        // Get the importDetails
        restImportDetailsMockMvc.perform(get("/api/import-details/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImportDetails() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        int databaseSizeBeforeUpdate = importDetailsRepository.findAll().size();

        // Update the importDetails
        ImportDetails updatedImportDetails = importDetailsRepository.findById(importDetails.getId()).get();
        // Disconnect from session so that the updates on updatedImportDetails are not directly saved in db
        em.detach(updatedImportDetails);
        updatedImportDetails
            .amount(UPDATED_AMOUNT)
            .importPrice(UPDATED_IMPORT_PRICE);
        ImportDetailsDTO importDetailsDTO = importDetailsMapper.toDto(updatedImportDetails);

        restImportDetailsMockMvc.perform(put("/api/import-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(importDetailsDTO)))
            .andExpect(status().isOk());

        // Validate the ImportDetails in the database
        List<ImportDetails> importDetailsList = importDetailsRepository.findAll();
        assertThat(importDetailsList).hasSize(databaseSizeBeforeUpdate);
        ImportDetails testImportDetails = importDetailsList.get(importDetailsList.size() - 1);
        assertThat(testImportDetails.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testImportDetails.getImportPrice()).isEqualTo(UPDATED_IMPORT_PRICE);

        // Validate the ImportDetails in Elasticsearch
        verify(mockImportDetailsSearchRepository, times(1)).save(testImportDetails);
    }

    @Test
    @Transactional
    public void updateNonExistingImportDetails() throws Exception {
        int databaseSizeBeforeUpdate = importDetailsRepository.findAll().size();

        // Create the ImportDetails
        ImportDetailsDTO importDetailsDTO = importDetailsMapper.toDto(importDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImportDetailsMockMvc.perform(put("/api/import-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(importDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ImportDetails in the database
        List<ImportDetails> importDetailsList = importDetailsRepository.findAll();
        assertThat(importDetailsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ImportDetails in Elasticsearch
        verify(mockImportDetailsSearchRepository, times(0)).save(importDetails);
    }

    @Test
    @Transactional
    public void deleteImportDetails() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);

        int databaseSizeBeforeDelete = importDetailsRepository.findAll().size();

        // Delete the importDetails
        restImportDetailsMockMvc.perform(delete("/api/import-details/{id}", importDetails.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ImportDetails> importDetailsList = importDetailsRepository.findAll();
        assertThat(importDetailsList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ImportDetails in Elasticsearch
        verify(mockImportDetailsSearchRepository, times(1)).deleteById(importDetails.getId());
    }

    @Test
    @Transactional
    public void searchImportDetails() throws Exception {
        // Initialize the database
        importDetailsRepository.saveAndFlush(importDetails);
        when(mockImportDetailsSearchRepository.search(queryStringQuery("id:" + importDetails.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(importDetails), PageRequest.of(0, 1), 1));
        // Search the importDetails
        restImportDetailsMockMvc.perform(get("/api/_search/import-details?query=id:" + importDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(importDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.[*].importPrice").value(hasItem(DEFAULT_IMPORT_PRICE.doubleValue())));
    }
}
