package com.ooad.bookstore.web.rest;


import com.ooad.bookstore.domain.ExportDetails;
import com.ooad.bookstore.domain.Book;
import com.ooad.bookstore.domain.ExportBook;
import com.ooad.bookstore.repository.ExportDetailsRepository;
import com.ooad.bookstore.repository.search.ExportDetailsSearchRepository;
import com.ooad.bookstore.service.ExportDetailsService;
import com.ooad.bookstore.service.dto.ExportDetailsDTO;
import com.ooad.bookstore.service.mapper.ExportDetailsMapper;
import com.ooad.bookstore.service.dto.ExportDetailsCriteria;
import com.ooad.bookstore.service.ExportDetailsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ExportDetailsResource} REST controller.
 */

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ExportDetailsResourceIT {

    private static final Integer DEFAULT_AMOUNT = 1;
    private static final Integer UPDATED_AMOUNT = 2;
    private static final Integer SMALLER_AMOUNT = 1 - 1;

    private static final Float DEFAULT_IMPORT_PRICE = 1F;
    private static final Float UPDATED_IMPORT_PRICE = 2F;
    private static final Float SMALLER_IMPORT_PRICE = 1F - 1F;

    @Autowired
    private ExportDetailsRepository exportDetailsRepository;

    @Autowired
    private ExportDetailsMapper exportDetailsMapper;

    @Autowired
    private ExportDetailsService exportDetailsService;

    /**
     * This repository is mocked in the com.ooad.bookstore.repository.search test package.
     *
     * @see com.ooad.bookstore.repository.search.ExportDetailsSearchRepositoryMockConfiguration
     */
    @Autowired
    private ExportDetailsSearchRepository mockExportDetailsSearchRepository;

    @Autowired
    private ExportDetailsQueryService exportDetailsQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExportDetailsMockMvc;

    private ExportDetails exportDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExportDetails createEntity(EntityManager em) {
        ExportDetails exportDetails = new ExportDetails()
            .amount(DEFAULT_AMOUNT)
            .importPrice(DEFAULT_IMPORT_PRICE);
        return exportDetails;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExportDetails createUpdatedEntity(EntityManager em) {
        ExportDetails exportDetails = new ExportDetails()
            .amount(UPDATED_AMOUNT)
            .importPrice(UPDATED_IMPORT_PRICE);
        return exportDetails;
    }

    @BeforeEach
    public void initTest() {
        exportDetails = createEntity(em);
    }

    @Test
    @Transactional
    public void createExportDetails() throws Exception {
        int databaseSizeBeforeCreate = exportDetailsRepository.findAll().size();

        // Create the ExportDetails
        ExportDetailsDTO exportDetailsDTO = exportDetailsMapper.toDto(exportDetails);
        restExportDetailsMockMvc.perform(post("/api/export-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(exportDetailsDTO)))
            .andExpect(status().isCreated());

        // Validate the ExportDetails in the database
        List<ExportDetails> exportDetailsList = exportDetailsRepository.findAll();
        assertThat(exportDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        ExportDetails testExportDetails = exportDetailsList.get(exportDetailsList.size() - 1);
        assertThat(testExportDetails.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testExportDetails.getImportPrice()).isEqualTo(DEFAULT_IMPORT_PRICE);

        // Validate the ExportDetails in Elasticsearch
        verify(mockExportDetailsSearchRepository, times(1)).save(testExportDetails);
    }

    @Test
    @Transactional
    public void createExportDetailsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = exportDetailsRepository.findAll().size();

        // Create the ExportDetails with an existing ID
        exportDetails.setId(1L);
        ExportDetailsDTO exportDetailsDTO = exportDetailsMapper.toDto(exportDetails);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExportDetailsMockMvc.perform(post("/api/export-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(exportDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ExportDetails in the database
        List<ExportDetails> exportDetailsList = exportDetailsRepository.findAll();
        assertThat(exportDetailsList).hasSize(databaseSizeBeforeCreate);

        // Validate the ExportDetails in Elasticsearch
        verify(mockExportDetailsSearchRepository, times(0)).save(exportDetails);
    }


    @Test
    @Transactional
    public void getAllExportDetails() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList
        restExportDetailsMockMvc.perform(get("/api/export-details?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exportDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.[*].importPrice").value(hasItem(DEFAULT_IMPORT_PRICE.doubleValue())));
    }

    @Test
    @Transactional
    public void getExportDetails() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get the exportDetails
        restExportDetailsMockMvc.perform(get("/api/export-details/{id}", exportDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(exportDetails.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT))
            .andExpect(jsonPath("$.importPrice").value(DEFAULT_IMPORT_PRICE.doubleValue()));
    }


    @Test
    @Transactional
    public void getExportDetailsByIdFiltering() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        Long id = exportDetails.getId();

        defaultExportDetailsShouldBeFound("id.equals=" + id);
        defaultExportDetailsShouldNotBeFound("id.notEquals=" + id);

        defaultExportDetailsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultExportDetailsShouldNotBeFound("id.greaterThan=" + id);

        defaultExportDetailsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultExportDetailsShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllExportDetailsByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where amount equals to DEFAULT_AMOUNT
        defaultExportDetailsShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the exportDetailsList where amount equals to UPDATED_AMOUNT
        defaultExportDetailsShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where amount not equals to DEFAULT_AMOUNT
        defaultExportDetailsShouldNotBeFound("amount.notEquals=" + DEFAULT_AMOUNT);

        // Get all the exportDetailsList where amount not equals to UPDATED_AMOUNT
        defaultExportDetailsShouldBeFound("amount.notEquals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultExportDetailsShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the exportDetailsList where amount equals to UPDATED_AMOUNT
        defaultExportDetailsShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where amount is not null
        defaultExportDetailsShouldBeFound("amount.specified=true");

        // Get all the exportDetailsList where amount is null
        defaultExportDetailsShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    public void getAllExportDetailsByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where amount is greater than or equal to DEFAULT_AMOUNT
        defaultExportDetailsShouldBeFound("amount.greaterThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the exportDetailsList where amount is greater than or equal to UPDATED_AMOUNT
        defaultExportDetailsShouldNotBeFound("amount.greaterThanOrEqual=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where amount is less than or equal to DEFAULT_AMOUNT
        defaultExportDetailsShouldBeFound("amount.lessThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the exportDetailsList where amount is less than or equal to SMALLER_AMOUNT
        defaultExportDetailsShouldNotBeFound("amount.lessThanOrEqual=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where amount is less than DEFAULT_AMOUNT
        defaultExportDetailsShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUNT);

        // Get all the exportDetailsList where amount is less than UPDATED_AMOUNT
        defaultExportDetailsShouldBeFound("amount.lessThan=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where amount is greater than DEFAULT_AMOUNT
        defaultExportDetailsShouldNotBeFound("amount.greaterThan=" + DEFAULT_AMOUNT);

        // Get all the exportDetailsList where amount is greater than SMALLER_AMOUNT
        defaultExportDetailsShouldBeFound("amount.greaterThan=" + SMALLER_AMOUNT);
    }


    @Test
    @Transactional
    public void getAllExportDetailsByImportPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where importPrice equals to DEFAULT_IMPORT_PRICE
        defaultExportDetailsShouldBeFound("importPrice.equals=" + DEFAULT_IMPORT_PRICE);

        // Get all the exportDetailsList where importPrice equals to UPDATED_IMPORT_PRICE
        defaultExportDetailsShouldNotBeFound("importPrice.equals=" + UPDATED_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByImportPriceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where importPrice not equals to DEFAULT_IMPORT_PRICE
        defaultExportDetailsShouldNotBeFound("importPrice.notEquals=" + DEFAULT_IMPORT_PRICE);

        // Get all the exportDetailsList where importPrice not equals to UPDATED_IMPORT_PRICE
        defaultExportDetailsShouldBeFound("importPrice.notEquals=" + UPDATED_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByImportPriceIsInShouldWork() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where importPrice in DEFAULT_IMPORT_PRICE or UPDATED_IMPORT_PRICE
        defaultExportDetailsShouldBeFound("importPrice.in=" + DEFAULT_IMPORT_PRICE + "," + UPDATED_IMPORT_PRICE);

        // Get all the exportDetailsList where importPrice equals to UPDATED_IMPORT_PRICE
        defaultExportDetailsShouldNotBeFound("importPrice.in=" + UPDATED_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByImportPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where importPrice is not null
        defaultExportDetailsShouldBeFound("importPrice.specified=true");

        // Get all the exportDetailsList where importPrice is null
        defaultExportDetailsShouldNotBeFound("importPrice.specified=false");
    }

    @Test
    @Transactional
    public void getAllExportDetailsByImportPriceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where importPrice is greater than or equal to DEFAULT_IMPORT_PRICE
        defaultExportDetailsShouldBeFound("importPrice.greaterThanOrEqual=" + DEFAULT_IMPORT_PRICE);

        // Get all the exportDetailsList where importPrice is greater than or equal to UPDATED_IMPORT_PRICE
        defaultExportDetailsShouldNotBeFound("importPrice.greaterThanOrEqual=" + UPDATED_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByImportPriceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where importPrice is less than or equal to DEFAULT_IMPORT_PRICE
        defaultExportDetailsShouldBeFound("importPrice.lessThanOrEqual=" + DEFAULT_IMPORT_PRICE);

        // Get all the exportDetailsList where importPrice is less than or equal to SMALLER_IMPORT_PRICE
        defaultExportDetailsShouldNotBeFound("importPrice.lessThanOrEqual=" + SMALLER_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByImportPriceIsLessThanSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where importPrice is less than DEFAULT_IMPORT_PRICE
        defaultExportDetailsShouldNotBeFound("importPrice.lessThan=" + DEFAULT_IMPORT_PRICE);

        // Get all the exportDetailsList where importPrice is less than UPDATED_IMPORT_PRICE
        defaultExportDetailsShouldBeFound("importPrice.lessThan=" + UPDATED_IMPORT_PRICE);
    }

    @Test
    @Transactional
    public void getAllExportDetailsByImportPriceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        // Get all the exportDetailsList where importPrice is greater than DEFAULT_IMPORT_PRICE
        defaultExportDetailsShouldNotBeFound("importPrice.greaterThan=" + DEFAULT_IMPORT_PRICE);

        // Get all the exportDetailsList where importPrice is greater than SMALLER_IMPORT_PRICE
        defaultExportDetailsShouldBeFound("importPrice.greaterThan=" + SMALLER_IMPORT_PRICE);
    }


    @Test
    @Transactional
    public void getAllExportDetailsByBookIsEqualToSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);
        Book book = BookResourceIT.createEntity(em);
        em.persist(book);
        em.flush();
        exportDetails.setBook(book);
        exportDetailsRepository.saveAndFlush(exportDetails);
        Long bookId = book.getId();

        // Get all the exportDetailsList where book equals to bookId
        defaultExportDetailsShouldBeFound("bookId.equals=" + bookId);

        // Get all the exportDetailsList where book equals to bookId + 1
        defaultExportDetailsShouldNotBeFound("bookId.equals=" + (bookId + 1));
    }


    @Test
    @Transactional
    public void getAllExportDetailsByExportBookIsEqualToSomething() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);
        ExportBook exportBook = ExportBookResourceIT.createEntity(em);
        em.persist(exportBook);
        em.flush();
        exportDetails.setExportBook(exportBook);
        exportDetailsRepository.saveAndFlush(exportDetails);
        Long exportBookId = exportBook.getId();

        // Get all the exportDetailsList where exportBook equals to exportBookId
        defaultExportDetailsShouldBeFound("exportBookId.equals=" + exportBookId);

        // Get all the exportDetailsList where exportBook equals to exportBookId + 1
        defaultExportDetailsShouldNotBeFound("exportBookId.equals=" + (exportBookId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultExportDetailsShouldBeFound(String filter) throws Exception {
        restExportDetailsMockMvc.perform(get("/api/export-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exportDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.[*].importPrice").value(hasItem(DEFAULT_IMPORT_PRICE.doubleValue())));

        // Check, that the count call also returns 1
        restExportDetailsMockMvc.perform(get("/api/export-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultExportDetailsShouldNotBeFound(String filter) throws Exception {
        restExportDetailsMockMvc.perform(get("/api/export-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restExportDetailsMockMvc.perform(get("/api/export-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingExportDetails() throws Exception {
        // Get the exportDetails
        restExportDetailsMockMvc.perform(get("/api/export-details/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExportDetails() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        int databaseSizeBeforeUpdate = exportDetailsRepository.findAll().size();

        // Update the exportDetails
        ExportDetails updatedExportDetails = exportDetailsRepository.findById(exportDetails.getId()).get();
        // Disconnect from session so that the updates on updatedExportDetails are not directly saved in db
        em.detach(updatedExportDetails);
        updatedExportDetails
            .amount(UPDATED_AMOUNT)
            .importPrice(UPDATED_IMPORT_PRICE);
        ExportDetailsDTO exportDetailsDTO = exportDetailsMapper.toDto(updatedExportDetails);

        restExportDetailsMockMvc.perform(put("/api/export-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(exportDetailsDTO)))
            .andExpect(status().isOk());

        // Validate the ExportDetails in the database
        List<ExportDetails> exportDetailsList = exportDetailsRepository.findAll();
        assertThat(exportDetailsList).hasSize(databaseSizeBeforeUpdate);
        ExportDetails testExportDetails = exportDetailsList.get(exportDetailsList.size() - 1);
        assertThat(testExportDetails.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testExportDetails.getImportPrice()).isEqualTo(UPDATED_IMPORT_PRICE);

        // Validate the ExportDetails in Elasticsearch
        verify(mockExportDetailsSearchRepository, times(1)).save(testExportDetails);
    }

    @Test
    @Transactional
    public void updateNonExistingExportDetails() throws Exception {
        int databaseSizeBeforeUpdate = exportDetailsRepository.findAll().size();

        // Create the ExportDetails
        ExportDetailsDTO exportDetailsDTO = exportDetailsMapper.toDto(exportDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExportDetailsMockMvc.perform(put("/api/export-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(exportDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ExportDetails in the database
        List<ExportDetails> exportDetailsList = exportDetailsRepository.findAll();
        assertThat(exportDetailsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ExportDetails in Elasticsearch
        verify(mockExportDetailsSearchRepository, times(0)).save(exportDetails);
    }

    @Test
    @Transactional
    public void deleteExportDetails() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);

        int databaseSizeBeforeDelete = exportDetailsRepository.findAll().size();

        // Delete the exportDetails
        restExportDetailsMockMvc.perform(delete("/api/export-details/{id}", exportDetails.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExportDetails> exportDetailsList = exportDetailsRepository.findAll();
        assertThat(exportDetailsList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ExportDetails in Elasticsearch
        verify(mockExportDetailsSearchRepository, times(1)).deleteById(exportDetails.getId());
    }

    @Test
    @Transactional
    public void searchExportDetails() throws Exception {
        // Initialize the database
        exportDetailsRepository.saveAndFlush(exportDetails);
        when(mockExportDetailsSearchRepository.search(queryStringQuery("id:" + exportDetails.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(exportDetails), PageRequest.of(0, 1), 1));
        // Search the exportDetails
        restExportDetailsMockMvc.perform(get("/api/_search/export-details?query=id:" + exportDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exportDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.[*].importPrice").value(hasItem(DEFAULT_IMPORT_PRICE.doubleValue())));
    }
}
