package com.ooad.bookstore.web.rest;


import com.ooad.bookstore.domain.ExportBook;
import com.ooad.bookstore.domain.Employee;
import com.ooad.bookstore.domain.Customer;
import com.ooad.bookstore.domain.ExportDetails;
import com.ooad.bookstore.repository.ExportBookRepository;
import com.ooad.bookstore.repository.search.ExportBookSearchRepository;
import com.ooad.bookstore.service.ExportBookService;
import com.ooad.bookstore.service.dto.ExportBookDTO;
import com.ooad.bookstore.service.mapper.ExportBookMapper;
import com.ooad.bookstore.service.dto.ExportBookCriteria;
import com.ooad.bookstore.service.ExportBookQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ExportBookResource} REST controller.
 */

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ExportBookResourceIT {

    private static final Instant DEFAULT_EXPORT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_EXPORT_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Float DEFAULT_TOTAL_AMOUNT = 1F;
    private static final Float UPDATED_TOTAL_AMOUNT = 2F;
    private static final Float SMALLER_TOTAL_AMOUNT = 1F - 1F;

    @Autowired
    private ExportBookRepository exportBookRepository;

    @Autowired
    private ExportBookMapper exportBookMapper;

    @Autowired
    private ExportBookService exportBookService;

    /**
     * This repository is mocked in the com.ooad.bookstore.repository.search test package.
     *
     * @see com.ooad.bookstore.repository.search.ExportBookSearchRepositoryMockConfiguration
     */
    @Autowired
    private ExportBookSearchRepository mockExportBookSearchRepository;

    @Autowired
    private ExportBookQueryService exportBookQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExportBookMockMvc;

    private ExportBook exportBook;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExportBook createEntity(EntityManager em) {
        ExportBook exportBook = new ExportBook()
            .exportDate(DEFAULT_EXPORT_DATE)
            .totalAmount(DEFAULT_TOTAL_AMOUNT);
        return exportBook;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExportBook createUpdatedEntity(EntityManager em) {
        ExportBook exportBook = new ExportBook()
            .exportDate(UPDATED_EXPORT_DATE)
            .totalAmount(UPDATED_TOTAL_AMOUNT);
        return exportBook;
    }

    @BeforeEach
    public void initTest() {
        exportBook = createEntity(em);
    }

    @Test
    @Transactional
    public void createExportBook() throws Exception {
        int databaseSizeBeforeCreate = exportBookRepository.findAll().size();

        // Create the ExportBook
        ExportBookDTO exportBookDTO = exportBookMapper.toDto(exportBook);
        restExportBookMockMvc.perform(post("/api/export-books")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(exportBookDTO)))
            .andExpect(status().isCreated());

        // Validate the ExportBook in the database
        List<ExportBook> exportBookList = exportBookRepository.findAll();
        assertThat(exportBookList).hasSize(databaseSizeBeforeCreate + 1);
        ExportBook testExportBook = exportBookList.get(exportBookList.size() - 1);
        assertThat(testExportBook.getExportDate()).isEqualTo(DEFAULT_EXPORT_DATE);
        assertThat(testExportBook.getTotalAmount()).isEqualTo(DEFAULT_TOTAL_AMOUNT);

        // Validate the ExportBook in Elasticsearch
        verify(mockExportBookSearchRepository, times(1)).save(testExportBook);
    }

    @Test
    @Transactional
    public void createExportBookWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = exportBookRepository.findAll().size();

        // Create the ExportBook with an existing ID
        exportBook.setId(1L);
        ExportBookDTO exportBookDTO = exportBookMapper.toDto(exportBook);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExportBookMockMvc.perform(post("/api/export-books")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(exportBookDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ExportBook in the database
        List<ExportBook> exportBookList = exportBookRepository.findAll();
        assertThat(exportBookList).hasSize(databaseSizeBeforeCreate);

        // Validate the ExportBook in Elasticsearch
        verify(mockExportBookSearchRepository, times(0)).save(exportBook);
    }


    @Test
    @Transactional
    public void getAllExportBooks() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList
        restExportBookMockMvc.perform(get("/api/export-books?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exportBook.getId().intValue())))
            .andExpect(jsonPath("$.[*].exportDate").value(hasItem(DEFAULT_EXPORT_DATE.toString())))
            .andExpect(jsonPath("$.[*].totalAmount").value(hasItem(DEFAULT_TOTAL_AMOUNT.doubleValue())));
    }

    @Test
    @Transactional
    public void getExportBook() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get the exportBook
        restExportBookMockMvc.perform(get("/api/export-books/{id}", exportBook.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(exportBook.getId().intValue()))
            .andExpect(jsonPath("$.exportDate").value(DEFAULT_EXPORT_DATE.toString()))
            .andExpect(jsonPath("$.totalAmount").value(DEFAULT_TOTAL_AMOUNT.doubleValue()));
    }


    @Test
    @Transactional
    public void getExportBooksByIdFiltering() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        Long id = exportBook.getId();

        defaultExportBookShouldBeFound("id.equals=" + id);
        defaultExportBookShouldNotBeFound("id.notEquals=" + id);

        defaultExportBookShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultExportBookShouldNotBeFound("id.greaterThan=" + id);

        defaultExportBookShouldBeFound("id.lessThanOrEqual=" + id);
        defaultExportBookShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllExportBooksByExportDateIsEqualToSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where exportDate equals to DEFAULT_EXPORT_DATE
        defaultExportBookShouldBeFound("exportDate.equals=" + DEFAULT_EXPORT_DATE);

        // Get all the exportBookList where exportDate equals to UPDATED_EXPORT_DATE
        defaultExportBookShouldNotBeFound("exportDate.equals=" + UPDATED_EXPORT_DATE);
    }

    @Test
    @Transactional
    public void getAllExportBooksByExportDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where exportDate not equals to DEFAULT_EXPORT_DATE
        defaultExportBookShouldNotBeFound("exportDate.notEquals=" + DEFAULT_EXPORT_DATE);

        // Get all the exportBookList where exportDate not equals to UPDATED_EXPORT_DATE
        defaultExportBookShouldBeFound("exportDate.notEquals=" + UPDATED_EXPORT_DATE);
    }

    @Test
    @Transactional
    public void getAllExportBooksByExportDateIsInShouldWork() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where exportDate in DEFAULT_EXPORT_DATE or UPDATED_EXPORT_DATE
        defaultExportBookShouldBeFound("exportDate.in=" + DEFAULT_EXPORT_DATE + "," + UPDATED_EXPORT_DATE);

        // Get all the exportBookList where exportDate equals to UPDATED_EXPORT_DATE
        defaultExportBookShouldNotBeFound("exportDate.in=" + UPDATED_EXPORT_DATE);
    }

    @Test
    @Transactional
    public void getAllExportBooksByExportDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where exportDate is not null
        defaultExportBookShouldBeFound("exportDate.specified=true");

        // Get all the exportBookList where exportDate is null
        defaultExportBookShouldNotBeFound("exportDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllExportBooksByTotalAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where totalAmount equals to DEFAULT_TOTAL_AMOUNT
        defaultExportBookShouldBeFound("totalAmount.equals=" + DEFAULT_TOTAL_AMOUNT);

        // Get all the exportBookList where totalAmount equals to UPDATED_TOTAL_AMOUNT
        defaultExportBookShouldNotBeFound("totalAmount.equals=" + UPDATED_TOTAL_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportBooksByTotalAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where totalAmount not equals to DEFAULT_TOTAL_AMOUNT
        defaultExportBookShouldNotBeFound("totalAmount.notEquals=" + DEFAULT_TOTAL_AMOUNT);

        // Get all the exportBookList where totalAmount not equals to UPDATED_TOTAL_AMOUNT
        defaultExportBookShouldBeFound("totalAmount.notEquals=" + UPDATED_TOTAL_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportBooksByTotalAmountIsInShouldWork() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where totalAmount in DEFAULT_TOTAL_AMOUNT or UPDATED_TOTAL_AMOUNT
        defaultExportBookShouldBeFound("totalAmount.in=" + DEFAULT_TOTAL_AMOUNT + "," + UPDATED_TOTAL_AMOUNT);

        // Get all the exportBookList where totalAmount equals to UPDATED_TOTAL_AMOUNT
        defaultExportBookShouldNotBeFound("totalAmount.in=" + UPDATED_TOTAL_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportBooksByTotalAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where totalAmount is not null
        defaultExportBookShouldBeFound("totalAmount.specified=true");

        // Get all the exportBookList where totalAmount is null
        defaultExportBookShouldNotBeFound("totalAmount.specified=false");
    }

    @Test
    @Transactional
    public void getAllExportBooksByTotalAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where totalAmount is greater than or equal to DEFAULT_TOTAL_AMOUNT
        defaultExportBookShouldBeFound("totalAmount.greaterThanOrEqual=" + DEFAULT_TOTAL_AMOUNT);

        // Get all the exportBookList where totalAmount is greater than or equal to UPDATED_TOTAL_AMOUNT
        defaultExportBookShouldNotBeFound("totalAmount.greaterThanOrEqual=" + UPDATED_TOTAL_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportBooksByTotalAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where totalAmount is less than or equal to DEFAULT_TOTAL_AMOUNT
        defaultExportBookShouldBeFound("totalAmount.lessThanOrEqual=" + DEFAULT_TOTAL_AMOUNT);

        // Get all the exportBookList where totalAmount is less than or equal to SMALLER_TOTAL_AMOUNT
        defaultExportBookShouldNotBeFound("totalAmount.lessThanOrEqual=" + SMALLER_TOTAL_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportBooksByTotalAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where totalAmount is less than DEFAULT_TOTAL_AMOUNT
        defaultExportBookShouldNotBeFound("totalAmount.lessThan=" + DEFAULT_TOTAL_AMOUNT);

        // Get all the exportBookList where totalAmount is less than UPDATED_TOTAL_AMOUNT
        defaultExportBookShouldBeFound("totalAmount.lessThan=" + UPDATED_TOTAL_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllExportBooksByTotalAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        // Get all the exportBookList where totalAmount is greater than DEFAULT_TOTAL_AMOUNT
        defaultExportBookShouldNotBeFound("totalAmount.greaterThan=" + DEFAULT_TOTAL_AMOUNT);

        // Get all the exportBookList where totalAmount is greater than SMALLER_TOTAL_AMOUNT
        defaultExportBookShouldBeFound("totalAmount.greaterThan=" + SMALLER_TOTAL_AMOUNT);
    }


    @Test
    @Transactional
    public void getAllExportBooksByEmployeeIsEqualToSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);
        Employee employee = EmployeeResourceIT.createEntity(em);
        em.persist(employee);
        em.flush();
        exportBook.setEmployee(employee);
        exportBookRepository.saveAndFlush(exportBook);
        Long employeeId = employee.getId();

        // Get all the exportBookList where employee equals to employeeId
        defaultExportBookShouldBeFound("employeeId.equals=" + employeeId);

        // Get all the exportBookList where employee equals to employeeId + 1
        defaultExportBookShouldNotBeFound("employeeId.equals=" + (employeeId + 1));
    }


    @Test
    @Transactional
    public void getAllExportBooksByCustomerIsEqualToSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);
        Customer customer = CustomerResourceIT.createEntity(em);
        em.persist(customer);
        em.flush();
        exportBook.setCustomer(customer);
        exportBookRepository.saveAndFlush(exportBook);
        Long customerId = customer.getId();

        // Get all the exportBookList where customer equals to customerId
        defaultExportBookShouldBeFound("customerId.equals=" + customerId);

        // Get all the exportBookList where customer equals to customerId + 1
        defaultExportBookShouldNotBeFound("customerId.equals=" + (customerId + 1));
    }


    @Test
    @Transactional
    public void getAllExportBooksByImportDetailsIsEqualToSomething() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);
        ExportDetails importDetails = ExportDetailsResourceIT.createEntity(em);
        em.persist(importDetails);
        em.flush();
        exportBook.addImportDetails(importDetails);
        exportBookRepository.saveAndFlush(exportBook);
        Long importDetailsId = importDetails.getId();

        // Get all the exportBookList where importDetails equals to importDetailsId
        defaultExportBookShouldBeFound("importDetailsId.equals=" + importDetailsId);

        // Get all the exportBookList where importDetails equals to importDetailsId + 1
        defaultExportBookShouldNotBeFound("importDetailsId.equals=" + (importDetailsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultExportBookShouldBeFound(String filter) throws Exception {
        restExportBookMockMvc.perform(get("/api/export-books?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exportBook.getId().intValue())))
            .andExpect(jsonPath("$.[*].exportDate").value(hasItem(DEFAULT_EXPORT_DATE.toString())))
            .andExpect(jsonPath("$.[*].totalAmount").value(hasItem(DEFAULT_TOTAL_AMOUNT.doubleValue())));

        // Check, that the count call also returns 1
        restExportBookMockMvc.perform(get("/api/export-books/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultExportBookShouldNotBeFound(String filter) throws Exception {
        restExportBookMockMvc.perform(get("/api/export-books?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restExportBookMockMvc.perform(get("/api/export-books/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingExportBook() throws Exception {
        // Get the exportBook
        restExportBookMockMvc.perform(get("/api/export-books/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExportBook() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        int databaseSizeBeforeUpdate = exportBookRepository.findAll().size();

        // Update the exportBook
        ExportBook updatedExportBook = exportBookRepository.findById(exportBook.getId()).get();
        // Disconnect from session so that the updates on updatedExportBook are not directly saved in db
        em.detach(updatedExportBook);
        updatedExportBook
            .exportDate(UPDATED_EXPORT_DATE)
            .totalAmount(UPDATED_TOTAL_AMOUNT);
        ExportBookDTO exportBookDTO = exportBookMapper.toDto(updatedExportBook);

        restExportBookMockMvc.perform(put("/api/export-books")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(exportBookDTO)))
            .andExpect(status().isOk());

        // Validate the ExportBook in the database
        List<ExportBook> exportBookList = exportBookRepository.findAll();
        assertThat(exportBookList).hasSize(databaseSizeBeforeUpdate);
        ExportBook testExportBook = exportBookList.get(exportBookList.size() - 1);
        assertThat(testExportBook.getExportDate()).isEqualTo(UPDATED_EXPORT_DATE);
        assertThat(testExportBook.getTotalAmount()).isEqualTo(UPDATED_TOTAL_AMOUNT);

        // Validate the ExportBook in Elasticsearch
        verify(mockExportBookSearchRepository, times(1)).save(testExportBook);
    }

    @Test
    @Transactional
    public void updateNonExistingExportBook() throws Exception {
        int databaseSizeBeforeUpdate = exportBookRepository.findAll().size();

        // Create the ExportBook
        ExportBookDTO exportBookDTO = exportBookMapper.toDto(exportBook);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExportBookMockMvc.perform(put("/api/export-books")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(exportBookDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ExportBook in the database
        List<ExportBook> exportBookList = exportBookRepository.findAll();
        assertThat(exportBookList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ExportBook in Elasticsearch
        verify(mockExportBookSearchRepository, times(0)).save(exportBook);
    }

    @Test
    @Transactional
    public void deleteExportBook() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);

        int databaseSizeBeforeDelete = exportBookRepository.findAll().size();

        // Delete the exportBook
        restExportBookMockMvc.perform(delete("/api/export-books/{id}", exportBook.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExportBook> exportBookList = exportBookRepository.findAll();
        assertThat(exportBookList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ExportBook in Elasticsearch
        verify(mockExportBookSearchRepository, times(1)).deleteById(exportBook.getId());
    }

    @Test
    @Transactional
    public void searchExportBook() throws Exception {
        // Initialize the database
        exportBookRepository.saveAndFlush(exportBook);
        when(mockExportBookSearchRepository.search(queryStringQuery("id:" + exportBook.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(exportBook), PageRequest.of(0, 1), 1));
        // Search the exportBook
        restExportBookMockMvc.perform(get("/api/_search/export-books?query=id:" + exportBook.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exportBook.getId().intValue())))
            .andExpect(jsonPath("$.[*].exportDate").value(hasItem(DEFAULT_EXPORT_DATE.toString())))
            .andExpect(jsonPath("$.[*].totalAmount").value(hasItem(DEFAULT_TOTAL_AMOUNT.doubleValue())));
    }
}
