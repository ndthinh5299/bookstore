package com.ooad.bookstore.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ooad.bookstore.web.rest.TestUtil;

public class ExportDetailsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExportDetailsDTO.class);
        ExportDetailsDTO exportDetailsDTO1 = new ExportDetailsDTO();
        exportDetailsDTO1.setId(1L);
        ExportDetailsDTO exportDetailsDTO2 = new ExportDetailsDTO();
        assertThat(exportDetailsDTO1).isNotEqualTo(exportDetailsDTO2);
        exportDetailsDTO2.setId(exportDetailsDTO1.getId());
        assertThat(exportDetailsDTO1).isEqualTo(exportDetailsDTO2);
        exportDetailsDTO2.setId(2L);
        assertThat(exportDetailsDTO1).isNotEqualTo(exportDetailsDTO2);
        exportDetailsDTO1.setId(null);
        assertThat(exportDetailsDTO1).isNotEqualTo(exportDetailsDTO2);
    }
}
