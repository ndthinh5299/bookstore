package com.ooad.bookstore.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ooad.bookstore.web.rest.TestUtil;

public class ExportBookDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExportBookDTO.class);
        ExportBookDTO exportBookDTO1 = new ExportBookDTO();
        exportBookDTO1.setId(1L);
        ExportBookDTO exportBookDTO2 = new ExportBookDTO();
        assertThat(exportBookDTO1).isNotEqualTo(exportBookDTO2);
        exportBookDTO2.setId(exportBookDTO1.getId());
        assertThat(exportBookDTO1).isEqualTo(exportBookDTO2);
        exportBookDTO2.setId(2L);
        assertThat(exportBookDTO1).isNotEqualTo(exportBookDTO2);
        exportBookDTO1.setId(null);
        assertThat(exportBookDTO1).isNotEqualTo(exportBookDTO2);
    }
}
