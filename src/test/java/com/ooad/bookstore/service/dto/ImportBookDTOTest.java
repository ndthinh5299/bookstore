package com.ooad.bookstore.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ooad.bookstore.web.rest.TestUtil;

public class ImportBookDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImportBookDTO.class);
        ImportBookDTO importBookDTO1 = new ImportBookDTO();
        importBookDTO1.setId(1L);
        ImportBookDTO importBookDTO2 = new ImportBookDTO();
        assertThat(importBookDTO1).isNotEqualTo(importBookDTO2);
        importBookDTO2.setId(importBookDTO1.getId());
        assertThat(importBookDTO1).isEqualTo(importBookDTO2);
        importBookDTO2.setId(2L);
        assertThat(importBookDTO1).isNotEqualTo(importBookDTO2);
        importBookDTO1.setId(null);
        assertThat(importBookDTO1).isNotEqualTo(importBookDTO2);
    }
}
