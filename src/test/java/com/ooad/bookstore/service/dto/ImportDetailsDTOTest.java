package com.ooad.bookstore.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ooad.bookstore.web.rest.TestUtil;

public class ImportDetailsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImportDetailsDTO.class);
        ImportDetailsDTO importDetailsDTO1 = new ImportDetailsDTO();
        importDetailsDTO1.setId(1L);
        ImportDetailsDTO importDetailsDTO2 = new ImportDetailsDTO();
        assertThat(importDetailsDTO1).isNotEqualTo(importDetailsDTO2);
        importDetailsDTO2.setId(importDetailsDTO1.getId());
        assertThat(importDetailsDTO1).isEqualTo(importDetailsDTO2);
        importDetailsDTO2.setId(2L);
        assertThat(importDetailsDTO1).isNotEqualTo(importDetailsDTO2);
        importDetailsDTO1.setId(null);
        assertThat(importDetailsDTO1).isNotEqualTo(importDetailsDTO2);
    }
}
