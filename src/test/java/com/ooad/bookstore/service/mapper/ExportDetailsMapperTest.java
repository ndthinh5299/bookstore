package com.ooad.bookstore.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ExportDetailsMapperTest {

    private ExportDetailsMapper exportDetailsMapper;

    @BeforeEach
    public void setUp() {
        exportDetailsMapper = new ExportDetailsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(exportDetailsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(exportDetailsMapper.fromId(null)).isNull();
    }
}
