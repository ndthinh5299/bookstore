package com.ooad.bookstore.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ImportBookMapperTest {

    private ImportBookMapper importBookMapper;

    @BeforeEach
    public void setUp() {
        importBookMapper = new ImportBookMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(importBookMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(importBookMapper.fromId(null)).isNull();
    }
}
