package com.ooad.bookstore.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ExportBookMapperTest {

    private ExportBookMapper exportBookMapper;

    @BeforeEach
    public void setUp() {
        exportBookMapper = new ExportBookMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(exportBookMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(exportBookMapper.fromId(null)).isNull();
    }
}
