package com.ooad.bookstore.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ImportDetailsMapperTest {

    private ImportDetailsMapper importDetailsMapper;

    @BeforeEach
    public void setUp() {
        importDetailsMapper = new ImportDetailsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(importDetailsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(importDetailsMapper.fromId(null)).isNull();
    }
}
