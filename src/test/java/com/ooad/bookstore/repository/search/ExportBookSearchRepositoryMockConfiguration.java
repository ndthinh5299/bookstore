package com.ooad.bookstore.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link ExportBookSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class ExportBookSearchRepositoryMockConfiguration {

    @MockBean
    private ExportBookSearchRepository mockExportBookSearchRepository;

}
